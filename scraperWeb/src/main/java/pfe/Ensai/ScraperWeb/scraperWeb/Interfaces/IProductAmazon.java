package pfe.Ensai.ScraperWeb.scraperWeb.Interfaces;

import java.util.Set;

public interface IProductAmazon{

	String scrapCategorie();
	Set<String> scrapAsinRecommandationsProducts();
	String scrapNbComments();
	String scrapRatingMean();
	String scrapClassement();
	String scrapAsin();
	String scrapDescription();
	Boolean scrapIsStock();
	Boolean scrapIsSoldByAmazon();
	Double scrapOtherPriceNew();
	Double scrapOtherPriceUsed();
	Double scrapOldPrice();
	Double scrapCurrentPrice();
	String scrapNameProduct();
	Set<String> scrapKeywords();
	String scrapBrand();


}
