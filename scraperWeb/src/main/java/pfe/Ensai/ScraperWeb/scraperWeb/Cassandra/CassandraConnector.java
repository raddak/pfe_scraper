package pfe.Ensai.ScraperWeb.scraperWeb.Cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;

public class CassandraConnector {
	/** Cassandra Cluster. */
	private Cluster cluster;

	/** Cassandra Session. */
	private Session session;

	//public static final String IP_ADDRESS = "localhost";
	public static final String IP_ADDRESS = "54.217.148.232";
	
	public static  final int PORT =9042;
	
	/**
	 * Main function for demonstrating connecting to Cassandra with host and port.
	 *
	 * @param args Command-line arguments; first argument, if provided, is the
	 *    host and second argument, if provided, is the port.
	 */
	public static void main(final String[] args)
	{
		final CassandraConnector client = new CassandraConnector();
		final String ipAddress = "localhost";
		final int port =9042;
		System.out.println("Connecting to IP Address " + ipAddress + ":" + port + "...");
		client.connect(ipAddress, port);
		client.close();
	}

	/**
	 * Connect to Cassandra Cluster specified by provided node IP
	 * address and port number.
	 *
	 * @param node Cluster node IP address.
	 * @param port Port of cluster host.
	 */
	public void connect(final String node, final int port)
	{
		this.cluster = Cluster.builder().addContactPoint(node).withPort(port).build();
		final Metadata metadata = cluster.getMetadata();
		System.out.println("Connected to cluster: %s\n"+ metadata.getClusterName());
		for (final Host host : metadata.getAllHosts())
		{
			System.out.println("Datacenter: %s; Host: %s; Rack: %s\n"+	host.getDatacenter()+ host.getAddress()+host.getRack());
		}
		session = cluster.connect();
	}

	/**
	 * Provide my Session.
	 *
	 * @return My session.
	 */
	public Session getSession()
	{
		return this.session;
	}

	/** Close cluster. */
	public void close()
	{
		cluster.close();
	}
	
}
