package pfe.Ensai.ScraperWeb.scraperWeb.Produit;

import java.util.Set;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;


@Table(keyspace = "PFE_KEYSPACE", name = "productsU")
public class ProduitU {
	
	private String URL=null;	
	@PartitionKey
	private String site=null;	
	@PartitionKey(value=1)
	private String reference=null;
	private String nom=null;
	private String marque=null;
	private String description=null;
	//Prix réel d'achat
	private Double prixVente=null;
	private Boolean isDisponible;
	private String EAN=null;
	private String categorie=null;
	private String sousCategorie=null;
	//Prix barré au autre
	private Double prixSolde=null;
	private Set<String> keyWords=null;
	private Double otherPriceNew=null;
	private Double otherPriceUsed=null;
	private Boolean isSoldbyAmazon=null;
	private String classement=null;
	private String ratingMean=null;
	private String nbComment=null;
	private Set<String> asinRecommandationProduct=null;


	public ProduitU() {
	}

	//Getters and Setters


	public String getURL() {
		return URL;
	}




	public String getSite() {
		return site;
	}




	public void setSite(String site) {
		this.site = site;
	}




	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrixVente() {
		return prixVente;
	}
	public void setPrixVente(Double prixVente) {
		this.prixVente = prixVente;
	}




	public Boolean getIsDisponible() {
		return isDisponible;
	}




	public void setIsDisponible(Boolean isDisponible) {
		this.isDisponible = isDisponible;
	}





	public String getEAN() {
		return EAN;
	}
	public void setEAN(String eAN) {
		EAN = eAN;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public String getSousCategorie() {
		return sousCategorie;
	}
	public void setSousCategorie(String sousCategorie) {
		this.sousCategorie = sousCategorie;
	}
	public Double getPrixSolde() {
		return prixSolde;
	}
	public void setPrixSolde(Double prixSolde) {
		this.prixSolde = prixSolde;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public Double getOtherPriceNew() {
		return otherPriceNew;
	}
	public void setOtherPriceNew(Double otherPriceNew) {
		this.otherPriceNew = otherPriceNew;
	}
	public Double getOtherPriceUsed() {
		return otherPriceUsed;
	}
	public void setOtherPriceUsed(Double otherPriceUsed) {
		this.otherPriceUsed = otherPriceUsed;
	}

	public String getClassement() {
		return classement;
	}
	public void setClassement(String classement) {
		this.classement = classement;
	}
	public String getRatingMean() {
		return ratingMean;
	}
	public void setRatingMean(String ratingMean) {
		this.ratingMean = ratingMean;
	}
	public String getNbComment() {
		return nbComment;
	}
	public void setNbComment(String nbComment) {
		this.nbComment = nbComment;
	}





	public Set<String> getKeyWords() {
		return keyWords;
	}




	public void setKeyWords(Set<String> keyWords) {
		this.keyWords = keyWords;
	}




	public Set<String> getAsinRecommandationProduct() {
		return asinRecommandationProduct;
	}




	public void setAsinRecommandationProduct(Set<String> asinRecommandationProduct) {
		this.asinRecommandationProduct = asinRecommandationProduct;
	}




	public void setIsSoldbyAmazon(Boolean isSoldbyAmazon) {
		this.isSoldbyAmazon = isSoldbyAmazon;
	}




	public Boolean getIsSoldbyAmazon() {
		return isSoldbyAmazon;
	}



}
