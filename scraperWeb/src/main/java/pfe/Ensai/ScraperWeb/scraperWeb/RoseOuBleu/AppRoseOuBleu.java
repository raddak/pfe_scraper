package pfe.Ensai.ScraperWeb.scraperWeb.RoseOuBleu;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.csvreader.CsvWriter;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;


public class AppRoseOuBleu implements IScraperProduct {

	public static void main(String[] args) throws SQLException, IOException {

		//get useful information
		Document doc = Jsoup.connect("http://www.roseoubleu.fr/").timeout(0).get();
		
		listerSections(doc);
		
		//retourneLiensProduits();
//		ArrayList<String> test = new ArrayList<String>();
//		test.add("http://www.roseoubleu.fr/Promenade/Poussettes-canne/");
//		ArrayList<String> test2 = new ArrayList<String>();
//		test2 =listerURLProduits(test);
//		
//		for(int i = 0;i < test2.size(); i++){
//			System.out.println(test2.get(i));
//		}

//		//		String test ="Test";
//
//		//		ArrayList<String> sections = new ArrayList<String>();
//		//		sections = listerSections(doc);
//		//		ArrayList<String> produits = new ArrayList<String>();
//		//		produits = 		listerProduits(sections);
//		//		for(int i = 0; i<produits.size(); i++){
//		//			recupDonnees(produits.get(i));
//		//		}
//
//
//		recupDonnees("http://www.roseoubleu.fr/Sieges-auto/Dos-a-la-route/CONCORD-Siege-auto-Reverso-Stone-Grey-1.html");
//		//System.out.println();
//		recupDonnees("http://www.roseoubleu.fr/Promenade/Porte-bebes/CYBEX-Porte-bebe-u-GO-Lollipop-Collection-2014-1.html");
//		//System.out.println();
//		recupDonnees("http://www.roseoubleu.fr/Promenade/Poussettes-canne/PEG-PEREGO-Pliko-Mini-Classico-Ghiro.html");
//		//System.out.println();
//		recupDonnees("http://www.roseoubleu.fr/Securite/Securite-enfants/REER-Bandes-antiderapantes-autocollantes-5m-80135-1.html");
	}

	public static ArrayList<String> listerSections(Document doc){
		ArrayList<String> retour = new ArrayList<String>();

		Elements section = doc.select("section[class]");
		for(Element sec: section){
			if(sec.attr("class").contains("top-bar-section")){
				Elements questions = sec.select("ul[class]");
				for(Element tit: questions){
					if(tit.attr("class").contains("dropdown layer")){
						Elements rep = tit.select("a[href]");
						for(Element link: rep){
							if(link.attr("href").contains("roseoubleu.fr") && !link.attr("href").contains("index")&& !link.attr("href").contains("Marques")&& !link.attr("href").contains("Boutique"))
								System.out.println(link.attr("abs:href"));
								retour.add(link.attr("abs:href"));
						}
					}
				}
			}
		}
System.out.println("Récupération des sections terminée");
		return retour  ;

	}

	public static ArrayList<String> listerURLProduits(ArrayList<String> listeSec) throws IOException{
		System.out.println("début listage produits");
		System.out.println("nombre de sections :"+listeSec.size());
		ArrayList<String> retour = new ArrayList<String>();

		for(String section:listeSec){
			retour.add(section);
			String passage = pagesDeProduits(section,retour);
			while (passage!=null){
				retour.add(passage);
				passage = pagesDeProduits(passage,retour);
				
			}

		}
		
		System.out.println(retour.toString());

		return retour;
	}

	public static String pagesDeProduits(String page, ArrayList<String> listeURL) throws IOException{
		String retour = null;
		Document doc = Jsoup.connect(page).timeout(0).get();

		Elements niv1 = doc.select("ul[id]");
		for(Element n1:niv1){
			if (n1.attr("id").contains("bmCategoryList")){
				Elements niv2 = n1.select("div[class]");
				for(Element n2:niv2){
					if(n2.attr("class").contains("h2")){
						Elements niv3 = n2.select("a[href]");
						for(Element n3:niv3){
							System.out.println("Voilà, je rajoute une URL, c'est la  "+listeURL.size()+" ème !");
							listeURL.add(n3.attr("abs:href"));
						}
					};
				}

			}
		}
		Elements testFin = doc.select("ul[class]");
		for(Element t:testFin){
			if(t.attr("class").contains("pagination")){
				Elements test2 = t.select("li[class]");
				for(Element t2:test2){
					if(t2.attr("class").contains("arrow") && !t2.attr("class").contains("unavailable")){
						Elements test3 = t2.select("a[href]");
						for(Element t3:test3){
							retour = t3.attr("abs:href");

						}
					}
				}
			}
		}
		if(retour!=null){
			retour = retour.replace("amp;", "");
		}
		//System.out.println("retour :"+retour);

		return retour;
	}

	public static String recupDonnees(String page) throws IOException{
		String retour = "";
		String categorie = ""; String sousCategorie = ""; String urlImage = "";String prixSolde="";String prixVente = "";
		String intitule = "";String reference = ""; String descripion = "";String disponibilite = "";
		Document doc = Jsoup.connect(page).timeout(0).get();

		Elements niv1 = doc.select("a");
		for(Element n1:niv1){
			if(n1.attr("class").contains("bccolor1")){
				categorie = n1.attr("title").replace("amp;", "");
				//retour = retour + n1.attr("title").replace("amp;", "");
			}
			if(n1.attr("class").contains("bccolor2")){
				sousCategorie = n1.attr("title").replace("amp;", "");
				//retour = retour + ";"+n1.attr("title").replace("amp;", "");

			}
			if(n1.attr("class").contains("infoImg")){
				urlImage = n1.attr("abs:href").replace("amp;", "");
				//retour = retour+";"+n1.attr("abs:href").replace("amp;", "");

			}

		}

		Elements niv2 = doc.select("div");
		for(Element n2:niv2){
			if(n2.attr("id").contains("productTitle")){
				intitule = n2.text();
				//retour = retour +";"+n2.text();
			}
			if(n2.attr("id").contains("productPrice")){
				Elements niv3 = n2.select("strong");
				for(Element n3:niv3){
					prixVente = n3.text().replace("€", "");
					//retour = retour + ";"+n3.text();
				}
			}
			if(n2.attr("class").contains("row oldPriceRow")){
				Elements niv3 = n2.select("del");
				for(Element n3:niv3){
					prixSolde = n3.text().replace("€", "");
					//retour = retour +";"+n3.text();
				}

			}
			if(n2.attr("class").contains("row stock")){
				Elements niv3 = n2.select("span");
				for(Element n3:niv3){
					if(n3.attr("class").contains("stockFlag")){
						disponibilite = n3.text();

					}

				}					
			}


		}

		Elements niv3 = doc.select("p");
		for(Element n3:niv3){
			if(n3.attr("class").contains("artNr")){
				reference = n3.text().replace("Numéro d'article : ", "");
			}

		}

		Elements niv4 = doc.select("div");
		for(Element n4:niv4){
			if(n4.attr("id").contains("detailsRelated")){
				Elements niv5 = n4.select("div");
				for(Element n5:niv5){
					if(n5.attr("class").contains("panel")){
						Elements niv6 = n5.select("div");
						for(Element n6:niv6){
							if(n6.attr("class").isEmpty()){
								descripion = n6.text();
							}
						}
					}
				}
			}

		}

		String ean = recupEAN(recupLienObjet(recupLienRecherche(intitule)));


		retour = categorie+";"+sousCategorie+";"+urlImage+";"+intitule+";"+prixVente+";"+prixSolde+";"+disponibilite+";"+reference+";"+descripion;
		System.out.println(retour);
		System.out.println(ean);
		ecritCsv("/home/perrine/essaiCSV", categorie, sousCategorie, urlImage, intitule, prixVente, prixSolde,disponibilite,reference,descripion);
		return retour;
	}

	public  ProduitD startScraper(String URL){
		ProduitD prod= new ProduitD(URL, "RoseOuBleu");

		Document doc = null;
		try {
			doc = Jsoup.connect(URL).timeout(0).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Elements niv1 = doc.select("a");
		for(Element n1:niv1){
			if(n1.attr("class").contains("bccolor1")){
				String categorie = n1.attr("title").replace("amp;", "");
				prod.setCategorie(categorie);
			}
			if(n1.attr("class").contains("bccolor2")){
				String sousCategorie = n1.attr("title").replace("amp;", "");
				prod.setSousCategorie(sousCategorie);
			}
		}
		Elements niv2 = doc.select("div");
		for(Element n2:niv2){
			if(n2.attr("id").contains("productTitle")){
				String intitule = n2.text();
				prod.setNom(intitule);

				String ean = recupEAN(recupLienObjet(recupLienRecherche(intitule)));
				prod.setEAN(ean);
			}
			if(n2.attr("id").contains("productPrice")){
				Elements niv3 = n2.select("strong");
				for(Element n3:niv3){
					String pVente = n3.text().replace("€", "");
					pVente = pVente.replace(",", ".");
					double prixVente = Double.parseDouble(pVente);
					prod.setPrixVente(prixVente);
				}
			}
			if(n2.attr("class").contains("row oldPriceRow")){
				Elements niv3 = n2.select("del");
				for(Element n3:niv3){
					String pSolde = n3.text().replace("€", "");
					pSolde = pSolde.replace(",", ".");
					double prixSolde = Double.parseDouble(pSolde);
					prod.setPrixSolde(prixSolde);
				}

			}
			if(n2.attr("class").contains("row stock")){
				Elements niv3 = n2.select("span");
				for(Element n3:niv3){
					if(n3.attr("class").contains("stockFlag")){
						//TODO check les valeurs de disponibilité !
						String disponibilite = n3.text();
						Boolean dispo = null;
						if(disponibilite.equals("Disponible")){
							dispo = true;
						}
						else {
							dispo = false;
						}
						prod.setIsDisponible(dispo);
					}
				}					
			}
		}
		Elements niv3 = doc.select("p");
		for(Element n3:niv3){
			if(n3.attr("class").contains("artNr")){
				String reference = n3.text().replace("Numéro d'article : ", "");
				prod.setReference(reference);
			}
		}

		Elements niv4 = doc.select("div");
		for(Element n4:niv4){
			if(n4.attr("id").contains("detailsRelated")){
				Elements niv5 = n4.select("div");
				for(Element n5:niv5){
					if(n5.attr("class").contains("panel")){
						Elements niv6 = n5.select("div");
						for(Element n6:niv6){
							if(n6.attr("class").isEmpty()){
								String descripion = n6.text();
								prod.setDescription(descripion);

							}
						}
					}
				}
			}

		}



		return prod;
	}

	public static void ecritCsv(String nomCSV,String categorie, String sousCategorie, String urlImage, String intitule, String prixVente, String prixSolde, String disponibilite, String reference,String descripion){
		String outputFile = nomCSV+".csv";

		// before we open the file check to see if it already exists
		boolean alreadyExists = new File(outputFile).exists();

		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');

			// if the file didn't already exist then we need to write out the header line
			if (!alreadyExists)
			{
				csvOutput.write("Categorie");
				csvOutput.write("Sous-Categorie");
				csvOutput.write("URLImage");
				csvOutput.write("Intitule");
				csvOutput.write("prixVente");
				csvOutput.write("prixSolde");
				csvOutput.write("Disponibilite");
				csvOutput.write("Reference");
				csvOutput.write("Description");

				csvOutput.endRecord();
			}
			// else assume that the file already has the correct header line

			// write out a few records
			csvOutput.write(categorie);
			csvOutput.write(sousCategorie);
			csvOutput.write(urlImage);
			csvOutput.write(intitule);
			csvOutput.write(prixVente);
			csvOutput.write(prixSolde);
			csvOutput.write(disponibilite);
			csvOutput.write(reference);
			csvOutput.write(descripion);
			csvOutput.endRecord();


			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String recupLienRecherche(String requ) {
		String requete = "https://www.eanfind.fr/chercher/";
		requete = requete +requ.replace("Collection 2014", "");
		requete = requete.replace(" ", "%20");
		//System.out.println(requete);
		return requete;
	}

	public static String recupLienObjet(String lienRequete) {
		Document doc = null;
		try {
			doc = Jsoup.connect(lienRequete).timeout(0).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String lienObj = "";
		if (doc != null) {
			Elements quellePage = doc.select("section");
			for(Element page:quellePage){
				if(page.attr("id").contains("main-content")){
					Elements el = doc.select("ul[class=search-result moz]");
					if (el.size() > 0) {
						Elements lis = el.get(0).select("li");
						lienObj = lis.get(0).select("a").attr("href");
					}
					Elements resultatNul = page.select("p");
					for(Element pp:resultatNul){
						if(pp.attr("class").contains("no-result")){
							lienObj="";
						}
					}
					
				}
				else if(page.attr("id").contains("full-content")){
					lienObj = lienRequete;
				}
			}

		}
		 //System.out.println("lien objet : " +lienObj);
		return lienObj;
	}

	public static String recupEAN(String lienObj) {
		String EAN = "";
		if (!lienObj.equals("")) {

			Document doc = null;

			try {
				doc = Jsoup.connect(lienObj).timeout(0).get();
			} catch (IOException e) {
				// TODO Auto-generated catch block

			}

			if (doc != null) {
				Elements el = doc.select("ul[class=more-infos]");
				if (el.size() > 0) {
					Elements lis = el.get(0).select("li");
					EAN = lis.get(0).select("div[class=span10]").text();
				}
			}
		}

		// System.out.println(EAN);

		return EAN;
	}
	
	public static ArrayList<String> retourneLiensProduits(){
		Document doc = null;
		try {
			doc = Jsoup.connect("http://www.roseoubleu.fr/").timeout(0).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<String> sections = new ArrayList<String>();
		sections = listerSections(doc);
		
		ArrayList<String> produits = new ArrayList<String>();
		try {
			produits = 		listerURLProduits(sections);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return produits;
		
	}
	

	


}