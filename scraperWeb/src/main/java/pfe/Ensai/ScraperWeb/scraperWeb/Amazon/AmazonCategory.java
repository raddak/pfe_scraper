package pfe.Ensai.ScraperWeb.scraperWeb.Amazon;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import pfe.Ensai.ScraperWeb.scraperWeb.Abstract.CategoryAbstract;

public class AmazonCategory extends CategoryAbstract{



	public final static String DIRECTORY_URL_PRODUCTS="./urlsProductsPerCategory/Amazon/";
	public final static String DIRECTORY_URL_CATEGORY="./urlsCategory/Amazon/";	

	public AmazonCategory(String url,String name){
		super(url,name);
	}

	public void writeURLProducts() {
		if (!urlProduct.isEmpty()) {


			File dir = new File(DIRECTORY_URL_PRODUCTS);
			dir.mkdirs();
			File file = new File(dir, "[URL]"+name+".txt");

			try {
				FileWriter  fileWriter=new FileWriter (file);
				for (String string : urlProduct) {

					fileWriter.write(string+"\n");
				}
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void writeURLAmazonCategory() {	

		File dir = new File(DIRECTORY_URL_CATEGORY);
		dir.mkdirs();
		File file = new File(dir, name+".txt");

		try {
			FileWriter  fileWriter=new FileWriter (file);			
			fileWriter.write(url+"\n");
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
