package pfe.Ensai.ScraperWeb.scraperWeb.Interfaces;

import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;

public interface IScraperProduct {
	
	ProduitD startScraper(String URL);

}
