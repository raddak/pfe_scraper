package pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pfe.Ensai.ScraperWeb.scraperWeb.Aubert.SoupAubert;
import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;

public class AppAubert extends AppQueueMulti {

	public static void main(String[] args) throws Exception {		

		List<String> tempStringList=new ArrayList<String>();
	
		
		
		//MultiThreading avec executor (share load of URLS):
		int nThreads = Runtime.getRuntime().availableProcessors();
	    System.out.println(nThreads);

		thread(new ConsumerProduitAsynchrone("AubertQueue"), false);
		
		ExecutorService executor = Executors.newFixedThreadPool(nThreads-1);
		
		
		IScraperProduct soupProductScraper=new SoupAubert();
		tempStringList=((SoupAubert)soupProductScraper).get_all_product_links(((SoupAubert)soupProductScraper).get_category_links());
		
		List<List<String>> subLists=MyPartition.partition(tempStringList, nThreads-1);
		for (List<String> list : subLists) {
            Runnable worker = new ProducerProduitSynchrone(list,soupProductScraper,"AubertQueue",false);
            executor.execute(worker);
		}
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
        
		
	}

}