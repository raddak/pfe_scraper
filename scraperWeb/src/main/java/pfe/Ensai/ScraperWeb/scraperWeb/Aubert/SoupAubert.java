package pfe.Ensai.ScraperWeb.scraperWeb.Aubert;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;
import au.com.bytecode.opencsv.CSVWriter;

// On utilise l'interface IScraperProduct qui est importante pour la fonction startScraper
public class SoupAubert implements IScraperProduct {

	// L'Url de base du site à scrapper
	String BASE_URL = "http://www.aubert.com";

	// Une fonction qui permet de récupérer les liens vers les pages des
	// différentes catégories du site
	public ArrayList<String> get_category_links() {
		ArrayList<String> liens = new ArrayList<String>();

		Document doc;
		try {
			doc = Jsoup.connect(BASE_URL).timeout(0).get();

			Elements el = doc.select("ul[class=tagged-menu-giant]");
			for (Element element : el) {
				Elements lis = element.select("li");
				for (Element li : lis) {
					String lien = li.select("a").attr("href");
					liens.add(lien);
				}
			}
		} catch (IOException e) {
			System.out
			.println("Les liens pour les catégories n'ont pas pu être récupérés.");
			e.printStackTrace();
		}
		System.out.println(liens);
		return liens;
	}

	public ArrayList<String> get_subcategory_links(String category_url) {
		ArrayList<String> liens = new ArrayList<String>();

		Document doc;
		try {
			doc = Jsoup.connect(category_url).timeout(0).get();

			Elements el = doc
					.select("h2[class=shelf-heading no-bottom-border]");
			for (Element element : el) {

				String lien = element.select("a").attr("href");
				// System.out.println(lien);
				liens.add(lien);
			}

			Elements el2 = doc.select("h2[class=shelf-heading]");
			for (Element element : el2) {

				String lien = element.select("a").attr("href");
				// System.out.println(lien);
				liens.add(lien);
			}

		} catch (IOException e) {
			System.out
			.println("Les liens pour les catégories n'ont pas pu être récupérés.");
			e.printStackTrace();
		}
		// System.out.println(liens);
		return liens;
	}

	public boolean testFond(String lien) {
		boolean res = false;
		Document doc;
		try {
			doc = Jsoup.connect(lien).timeout(0).get();
			Elements el = doc
					.select("h2[class=shelf-heading no-bottom-border]");
			Elements el2 = doc.select("h2[class=shelf-heading]");
			// System.out.println(el.size()+el2.size());
			if (el.size() + el2.size() == 0) {
				res = true;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	public ArrayList<String> get_liste_subcat(String categoryLink) {
		Document doc;
		// doc = Jsoup.connect(categoryLink).timeout(0).get();
		ArrayList<String> listeSubcats = new ArrayList<String>();

		ArrayList<String> subcats = new ArrayList<String>();

		subcats = get_subcategory_links(categoryLink);
		// System.out.println(subcats);
		listeSubcats = get_subcategory_links(categoryLink);
		//System.out.println(subcats);
		int compteur = 0;
		for (int i = 0; i < subcats.size(); i++) {
			boolean test = testFond(subcats.get(i));

			//System.out.println("Test sur " + subcats.get(i) + "Resultat:"
			// + test);

			if (!test) {
				// System.out.println(i);
				// System.out.println(listeSubcats);
				listeSubcats.addAll(get_subcategory_links(subcats.get(i)));
				listeSubcats.remove(i - compteur);
				compteur++;
				//System.out.println(listeSubcats);
			}
		}
		//System.out.println("Sortie de get liste");
		return listeSubcats;
	}

	public ArrayList<String> get_pages_url(String subca_link) {
		ArrayList<String> liens = new ArrayList<String>();
		liens.add(subca_link);
		boolean test = true;
		Document doc;
		try {
			String link = subca_link;
			while (test) {
				//System.out.println(link);
				doc = Jsoup.connect(link).timeout(0).get();
				// System.out.println(link);

				if (!doc.select("a[class=link pn-next]").attr("href")
						.equals("#")) {
					// System.out.println("lien: "+
					// doc.select("a[class=link pn-next]").attr("href"));
					link = doc.select("a[class=link pn-next]").attr("href");
					liens.add(link);
					// System.out.println(link);

				} else {
					test = false;
				}
			}
		} catch (IOException e) {
			// System.out.println("Les liens vers les produits n'ont pu être récupérés pour cette catégorie"+category_url);
			e.printStackTrace();
		}

		return liens;
	}

	// Une fonction prenant en entrée le lien d'une catégorie et qui retourne la
	// liste des liens vers les objets de cette catégorie.
	public ArrayList<String> get_product_page_links(String page_url) {
		// System.out.println(category_url);
		ArrayList<String> liens = new ArrayList<String>();

		Document doc;
		try {
			doc = Jsoup.connect(page_url).timeout(0).get();

			Elements el = doc.select("div[class=item-trigger]");
			for (Element element : el) {
				String lien = element.select("a").attr("href");
				// System.out.println(lien);
				liens.add(lien);
			}
		} catch (IOException e) {
			// System.out.println("Les liens vers les produits n'ont pu être récupérés pour cette catégorie"+category_url);
			e.printStackTrace();
		}
		// System.out.println(""+liens.size());
		return liens;
	}

	// Une fonction permettant de récupérer les données d'un produit à partir de
	// son URL et qui les retourne sous forme d'arrayList de string
	public ArrayList<String> get_data(String product_url) throws IOException {
		ArrayList<String> donnees = new ArrayList<String>();

		if (product_url != "") {
			Document doc = Jsoup.connect(product_url).timeout(0).get();

			String nom = doc.select("strong").get(1).text();
			String ref = doc.select("div[class=product-detail]").attr(
					"data-sku");
			String prix = doc.select("meta[itemprop=price]").attr("content");
			String marque = doc.select("meta[itemprop=brand]").attr("content");
			System.out.println(product_url);
			String description = doc.select("div[itemprop=description]").get(0)
					.text();
			description = description.replace("(Lire...)", "");
			String dispo = doc.select("meta[itemprop=availability]").attr(
					"content");
			String benefPrix = "";

			Elements tablePrix = doc.select("div[class=subrow-one]");
			Elements div = tablePrix.select("div[itemprop=offerDetails]");
			Elements table = div.select("table[class=table-price-information]");
			Elements benefPrice = table
					.select("span[class=benefit-price-amount]");
			benefPrix = benefPrice.text();

			benefPrix = benefPrix.replace("€", ".");
			String lienRech = recupLienRecherche(nom + " " + marque);
			String ean = "";
			ean = recupEAN(lienRech);
			if (ean.equals("")) {
				String lienObj = recupLienObjet(lienRech);
				ean = recupEAN(lienObj);
			}

			Elements threaditem = doc.select("div[class=thread-item]");
			Element categorie = threaditem.get(0);
			String categorietexte = categorie.text();
			Element souscate = threaditem.get(1);
			String souscatetexte = souscate.text();
			System.out.println(categorietexte + " " + souscatetexte);
			// System.out.println(description);
			donnees.add(nom);
			donnees.add(ref);
			donnees.add(marque);
			donnees.add(prix);
			donnees.add(benefPrix);
			donnees.add(description);
			donnees.add(dispo);
			donnees.add(ean);
			// System.out.println(ref + " " + nom + " " + prix + " " + marque);
		}
		return donnees;

	}

	// Fonction permettant de créer la requete pour la récup d'ean
	public String recupLienRecherche(String nomMarque) {
		String requete = "https://www.eanfind.fr/chercher/";
		requete += nomMarque.replace(" ", "%20");
		return requete;
	}

	// Fonction permettant de récupérer le lien vers un objet à partir d'une
	// requête sur eanFind
	public String recupLienObjet(String lienRequete) {
		Document doc = null;
		try {
			doc = Jsoup.connect(lienRequete).timeout(0).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String lienObj = "";
		if (doc != null) {
			Elements el = doc.select("ul[class=search-result moz]");
			if (el.size() > 0) {
				Elements lis = el.get(0).select("li");
				lienObj = lis.get(0).select("a").attr("href");
			}
		}
		//System.out.println(lienObj);
		return lienObj;
	}

	// Fonction permettant de récupérer l'ean d'un objet à partir de son url.
	public String recupEAN(String lienObj) {
		String EAN = "";
		if (!lienObj.equals("")) {

			Document doc = null;

			try {
				doc = Jsoup.connect(lienObj).timeout(0).get();
			} catch (IOException e) {
				System.out.println("La connection n'a pas fonctionné "
						+ lienObj);

			}

			if (doc != null) {
				Elements el = doc.select("ul[class=more-infos]");
				if (el.size() > 0) {
					Elements lis = el.get(0).select("li");
					EAN = lis.get(0).select("div[class=span10]").text();
				}
			}
		}

		// System.out.println(EAN);

		return EAN;
	}

	// Fonction permettant de scrapper aubert et d'écrire le résultat dans un
	// csv appelé Essai
	public void ecrire() throws IOException {
		String csv = "Essai";
		CSVWriter writer = new CSVWriter(new FileWriter(csv));
		String init = "Nom#Ref#Marque#Prix#Description#URL#Disponibilité#EAN";
		writer.writeNext(init.split("#"));

		ArrayList<String> cates = get_category_links();
		for (String cate : cates) {
			// System.out.println(cate);
			ArrayList<String> produits = get_product_page_links(cate);
			for (String prod : produits) {
				ArrayList<String> datas = get_data(prod);
				if (datas.size() > 2) {
					String ligne = datas.get(0) + "#" + datas.get(1) + "#"
							+ datas.get(2) + "#" + datas.get(3) + "#"
							+ datas.get(5) + "#" + prod + "#" + datas.get(6)
							+ "#" + datas.get(7);
					writer.writeNext(ligne.split("#"));

				}
			}
		}

	}

	// Fonction permettant de récupérer tous les liens des produits.
	public ArrayList<String> get_all_product_links(
			ArrayList<String> category_links) {
		ArrayList<String> liste = new ArrayList<String>();
		for (String cate : category_links) {
			System.out.println("Categorie " + cate);
			ArrayList<String> subca = new ArrayList<String>();
			subca = get_liste_subcat(cate);
			//System.out.println(subca.size());
			if(subca.size()>0){
				for(String subcat : subca){
					//System.out.println("Sous Categorie " + subcat);
					ArrayList<String> pages = new ArrayList<String>();
					pages=get_pages_url(subcat);
					for(String page : pages){
						ArrayList<String> prod = new ArrayList<String>();
						prod = get_product_page_links(page);
						for (String produitURL : prod) {
							liste.add(produitURL);
						}

					}



				}
			}else{
				ArrayList<String> pages = new ArrayList<String>();
				pages=get_pages_url(cate);
				for(String page : pages){
					ArrayList<String> prod = new ArrayList<String>();
					prod = get_product_page_links(page);
					for (String produitURL : prod) {
						liste.add(produitURL);
					}

				}
			}
		}
		return liste;
	}

	// Fonction permettant à partir de l'url d'un produit de récupérer les
	// données et renvoyant un produit.
	public ProduitD startScraper(String product_url) {
		ProduitD prod = new ProduitD(product_url, "Aubert");
		if (product_url != "") {
			try {
				Document doc = Jsoup.connect(product_url).timeout(0).get();

				String nom = doc.select("strong").get(1).text();
				String ref = doc.select("div[class=product-detail]").attr(
						"data-sku");
				String prix = doc.select("meta[itemprop=price]")
						.attr("content");
				String marque = doc.select("meta[itemprop=brand]").attr(
						"content");
				String description = doc.select("div[itemprop=description]")
						.get(0).text();
				description = description.replace("(Lire...)", "");
				String dispo = doc.select("meta[itemprop=availability]").attr(
						"content");
				String benefPrix = "";

				Elements tablePrix = doc.select("div[class=subrow-one]");
				Elements div = tablePrix.select("div[itemprop=offerDetails]");
				Elements table = div
						.select("table[class=table-price-information]");
				Elements benefPrice = table
						.select("span[class=benefit-price-amount]");
				benefPrix = benefPrice.text();

				benefPrix = benefPrix.replace("€", ".");
				String lienRech = recupLienRecherche(nom + " " + marque);
				String ean = "";
				ean = recupEAN(lienRech);
				if (ean.equals("")) {
					String lienObj = recupLienObjet(lienRech);
					ean = recupEAN(lienObj);
				}
				// System.out.println(description);

				Elements threaditem = doc.select("div[class=thread-item]");
				Element categorie = threaditem.get(0);
				String categorietexte = categorie.text();
				Element souscate = threaditem.get(1);
				String souscatetexte = souscate.text();

				boolean stock = true;
				Double prixNum = Double.parseDouble(prix);
				if (dispo.equals("out_of_stock")) {
					stock = false;
				}
				prod.setDescription(description);
				prod.setNom(nom);
				prod.setMarque(marque);
				prod.setReference(ref);
				prod.setEAN(ean);
				prod.setPrixVente(prixNum);
				prod.setIsDisponible(stock);
				prod.setCategorie(categorietexte);
				// System.out.println(ref + " " + nom + " " + prix + " " +
				// marque);
			}

			catch (IOException e1) {
				System.out.println("StartScraper n'a pas pu se connecter");
				e1.printStackTrace();
				prod = null;

			}
		}
		return prod.getNom() != null ? prod : null;

	}

}