package pfe.Ensai.ScraperWeb.scraperWeb.Amazon;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IProductAmazon;
import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;

public class AmazonProductScraper  implements IProductAmazon, IScraperProduct {


	protected Document doc;	

	protected static  Pattern PRICE_PATTERN = Pattern.compile("EUR \\d+(,\\d{1,2})?");
	protected static  Pattern RANK_PATTERN = Pattern.compile("((\\d.)?\\d+) e");
	
	public AmazonProductScraper(){
	}

	public ProduitD startScraper(String URL) {
		try {
			doc=Jsoup.connect(URL).timeout(1000).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)").get();
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

		ProduitD prodTemp=new ProduitD(URL, "Amazon");
		prodTemp.setNom(scrapNameProduct());
		prodTemp.setKeyWords(scrapKeywords());
		prodTemp.setMarque(scrapBrand());
		prodTemp.setPrixVente(scrapCurrentPrice());
		prodTemp.setPrixSolde(scrapOldPrice());
		prodTemp.setOtherPriceNew(scrapOtherPriceNew());
		prodTemp.setOtherPriceUsed(scrapOtherPriceUsed());
		prodTemp.setIsSoldbyAmazon(scrapIsSoldByAmazon());
		prodTemp.setIsDisponible(scrapIsStock());
		prodTemp.setDescription(scrapDescription());
		prodTemp.setClassement(scrapClassement());
		prodTemp.setRatingMean(scrapRatingMean());
		prodTemp.setNbComment(scrapNbComments());
		prodTemp.setAsinRecommandationProduct(scrapAsinRecommandationsProducts());
		prodTemp.setCategorie(scrapCategorie());
		prodTemp.setReference(scrapAsin());
		
		if (prodTemp.getReference()==null) {
			String temp=URL.substring(URL.indexOf("/B0")+1, URL.indexOf("/ref"));
			System.err.println(temp);
			prodTemp.setReference(temp);
		}
				
		return prodTemp;
	}

	public String scrapCategorie() {
		Element categorieElement=doc.getElementById("wayfinding-breadcrumbs_feature_div");

		return categorieElement!=null? categorieElement.text(): null;
	}


	public Set<String> scrapAsinRecommandationsProducts() {
		Set<String> resultsSet=new HashSet<String>();
		Elements asinRecommandationsElements =doc.select("div[data-asin]");
		for (Element element : asinRecommandationsElements) {
			resultsSet.add(element.attr("data-asin"));
		}
		return !resultsSet.isEmpty()? resultsSet:null;
	}


	public String scrapNbComments() {
		String nbComments=null;
		Element ratingMeanElement=doc.getElementById("summaryStars");
		Elements tempElements;
		if (ratingMeanElement==null) {
			ratingMeanElement=doc.getElementById("acr");
			if (ratingMeanElement!=null) {
				tempElements=ratingMeanElement.select("div[id^=acr-dpReviewsSummary]");
				if (!tempElements.isEmpty()) {
					ratingMeanElement=tempElements.first();
					nbComments=ratingMeanElement.text().substring(ratingMeanElement.text().indexOf("sur")+4, ratingMeanElement.text().indexOf("commentaires")-1);
				}


			}
		}
		else {
			nbComments=ratingMeanElement.text().replace("(", "").replace(")","");
		}

		return nbComments;
	}


	public String scrapRatingMean() {

		Element ratingMeanElement=doc.getElementById("avgRating");		
		Elements tempElements;
		if (ratingMeanElement==null) {
			ratingMeanElement=doc.getElementById("acr");
			if (ratingMeanElement!=null) {
				tempElements=ratingMeanElement.select("div[id^=acr-dpReviewsSummary]");
				if(!tempElements.isEmpty()){
					tempElements.first();
				}
			}
		}


		return ratingMeanElement!=null? ratingMeanElement.text().substring(0, ratingMeanElement.text().indexOf("étoiles")-1) : null;
	}


	public String scrapClassement() {
		String classement="";
		Element tempElement=doc.getElementById("SalesRank");
		if (tempElement!=null) {
			String tempString=tempElement.text();

			Matcher m = RANK_PATTERN.matcher(tempString);
			if (m.find()) {
				classement+=m.group(0).substring(0, m.group(0).length()-2);
			}

			//nombre \\d* en
			//split n°
			String[] tempArrayString=tempString.split("n°");
			for (int i = 1; i < tempArrayString.length; i++) {
				classement+=" / "+tempArrayString[i];
			}
		}		

		return !classement.equals("")? classement:null;
	}


	public String scrapAsin() {		
		String asin=null;
		Element asinElement=doc.getElementById("ASIN");	
		
		if (asinElement!=null) {
			asin= asinElement.attr("value");
		}		
		
		return asin;
	}


	public String scrapDescription() {

		Element descriptionElement=doc.getElementById("productDescription");

		if (descriptionElement==null) {
			descriptionElement=doc.getElementById("feature-bullets");
		}		

		return descriptionElement!=null? descriptionElement.text().replace("Descriptions du produit","").replace("Amazon.fr", "") : null;
	}


	public Boolean scrapIsStock() {

		Boolean isStock=null;
		String tempString="";
		Element isStockElement=doc.getElementById("availability");

		if (isStockElement==null) {
			Elements isStockElements=doc.select("div.buying");						
			Elements tempElements;
			for (Element element : isStockElements) {					
				tempElements=element.getElementsMatchingText("(v|V)endu par");
				if (tempElements!=null) {
					tempString=tempElements.text();
					if (!tempString.equals("")) {
						break;
					}
				}

			};
		}
		else {
			tempString=isStockElement.text();
		}
		if (!tempString.equals("")) {
			isStock=tempString.matches(".*(E|e)n stock.*");		
		}
		else {
			isStock=false;
		}

		return isStock;
	}


	public Boolean scrapIsSoldByAmazon() {
		Boolean isSoldByAmazon=null;
		String tempString="";
		Element isSoldByAmazonElement=doc.getElementById("merchant-info");

		if (isSoldByAmazonElement==null) {
			Elements isSoldByAmazonElements=doc.select("div.buying");			
			Elements tempElements;
			for (Element element : isSoldByAmazonElements) {					
				tempElements=element.getElementsMatchingText("(v|V)endu par");
				if (tempElements!=null) {
					tempString=tempElements.text();
					if (!tempString.equals("")) {
						break;
					}
				}

			};
		}
		else {
			tempString=isSoldByAmazonElement.text();
		}

		if (!tempString.equals("")) {
			isSoldByAmazon=tempString.matches(".*(v|V)endu par Amazon.*");		
		}
		else {
			isSoldByAmazon=false;
		}

		return isSoldByAmazon;
	}


	public Double scrapOtherPriceNew() {

		String otherPriceNew=null;
		Double otherPriceNewReturn=null;
		Element otherPriceNewElement=doc.getElementById("olpDivId");

		if (otherPriceNewElement==null) {
			otherPriceNewElement=doc.getElementById("olp_feature_div");
		}

		if (otherPriceNewElement!=null) {
			Matcher m = PRICE_PATTERN.matcher(otherPriceNewElement.text());
			if (m.find()) {
				otherPriceNew=m.group(0).replace("EUR", "").replace(",", ".");
				otherPriceNewReturn=Double.parseDouble(otherPriceNew);
			}
		}
		
		return otherPriceNewReturn;
	}

	public Double scrapOtherPriceUsed() {

		String otherPriceUsed=null;
		Double otherPriceUsedReturn=null;
		Element otherPriceUsedElement=doc.getElementById("olpDivId");

		if (otherPriceUsedElement==null) {
			otherPriceUsedElement=doc.getElementById("olp_feature_div");
		}

		if (otherPriceUsedElement!=null) {			
			Matcher m = PRICE_PATTERN.matcher(otherPriceUsedElement.text());
			int i=0;
			while (m.find() && i<2 ) {				
				if (i==1) {
					otherPriceUsed=m.group().replace("EUR", "").replace(",", ".");
					otherPriceUsedReturn=Double.parseDouble(otherPriceUsed);
				}
				i++;
			}

		}

		return otherPriceUsedReturn;
	}

	public Double scrapOldPrice() {

		String oldPrice=null;
		Double oldPriceReturn=null;
		Element oldPriceElement=doc.getElementById("listPriceValue");

		if (oldPriceElement==null) {				
			Elements oldPriceElements;
			oldPriceElement=doc.getElementById("price");
			if (oldPriceElement!=null) {
				oldPriceElements=oldPriceElement.getElementsMatchingText("\\d+(,\\d{1,2})?$");
				if (oldPriceElements!=null) {
					oldPriceElement=oldPriceElements.first();					
				}

			}
		}

		if (oldPriceElement!=null) {
			Matcher m = PRICE_PATTERN.matcher(oldPriceElement.text());
			if (m.find()) {
				oldPrice=m.group(0).replace("EUR", "").replace(",", ".");
				oldPriceReturn=Double.parseDouble(oldPrice);
			}
		}

		return oldPriceReturn;
	}


	public Double scrapCurrentPrice() {
		String currentPrice=null;
		Double currentPriceReturn=null;
		Element currentPriceElement =doc.getElementById("priceblock_ourprice");

		if (currentPriceElement==null) {			
			currentPriceElement=doc.getElementById("actualPriceValue");
		}

		if (currentPriceElement!=null) {
			Matcher m = PRICE_PATTERN.matcher(currentPriceElement.text());
			if (m.find()) {
				currentPrice=m.group(0).replace("EUR", "").replace(",", ".");
				currentPriceReturn=Double.parseDouble(currentPrice);
			}
		}

		return currentPriceReturn;
	}


	public String scrapNameProduct(){
		String nameProduct=null;
		Element element=doc.select("meta[name=title]").first();
		if (element!=null) {
			//First element
			nameProduct=element.attr("content");
			if (nameProduct!=null) {
			if (nameProduct.indexOf(": Amazon")!=-1) {
				nameProduct=nameProduct.substring(0,nameProduct.indexOf(": Amazon"));
			}
			else {
				nameProduct.replace("Amazon.fr", "");
			}
			
			}
		}
		return nameProduct;
	}


	public Set<String> scrapKeywords(){		

		Set<String> keyWords= new HashSet<String>();
		Elements keyWordsElements=doc.select("meta[name=keywords]");		
		Element keyWordsElement;
		if (!keyWordsElements.isEmpty()) {
			keyWordsElement=keyWordsElements.first();
			String[] temp=keyWordsElement.attr("content").split(",");
			for (int i = 0; i < temp.length; i++) {
				keyWords.add(temp[i]);
			}
		}
		return !keyWords.isEmpty()? keyWords:null;
	}

	public String scrapBrand(){		
		Element brandElement=doc.getElementById("brand");			
		if (brandElement==null) {
			Element brElement;
			Elements brElements;
			brElement=doc.getElementById("btAsinTitle");
			if (brElement!=null) {
				brElement=brElement.parent();
				if (brElement!=null) {
					brElement=brElement.parent();
					if (brElement!=null) {
						brElements=brElement.getElementsContainingOwnText("de");
						if (!brElements.select("a").isEmpty()) {
							brElement=brElement.select("a").first();
							brandElement=brElement;
						}
					}
				}
			}
			
		}

		return brandElement!=null? brandElement.text() : null ;
	}

	/*public String toString() {
		String toString="Url :"+getURL()+"\n"
				+"ASIN :"+getASIN()+"\n"
				+"nameProduct :"+getNom()+"\n"
				+"brand :"+getMarque()+"\n"
				+"keyWords :"+getKeyWords()+"\n"
				+"currentPrice :"+getPrixVente()+"\n"
				+"oldPrice :"+getPrixSolde()+"\n"
				+"otherPriceNew :"+getOtherPriceNew()+"\n"
				+"otherPriceUsed :"+getOtherPriceUsed()+"\n"
				+"isSoldByAmazon :"+getIsSoldbyAmazon()+"\n"
				+"isStock :"+getIsDisponible()+"\n"
				+"Description :"+getDescription()+"\n"
				+"Classement :\n"+getClassement()+"\n"
				+"ratingMean :"+getRatingMean()+"\n"
				+"nbComments :"+getNbComment()+"\n"
				+"asinRecommandationsProducts :"+getAsinRecommandationProduct()+"\n"
				+"category :"+getCategorie()+"\n";

		return toString;
	}*/





}

