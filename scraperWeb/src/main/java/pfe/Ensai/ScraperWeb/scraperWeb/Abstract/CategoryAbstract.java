package pfe.Ensai.ScraperWeb.scraperWeb.Abstract;

import java.util.HashSet;
import java.util.Set;

public abstract class CategoryAbstract {
	protected String url;
	protected String name;
	protected Set<String> urlProduct;

	public final static String DIRECTORY_URL_PRODUCTS="./urlsProductsPerCategory/";
	public final static String DIRECTORY_URL_CATEGORY="./urlsCategory/";

	public CategoryAbstract(String url,String name){
		this.url=url;
		this.name=name;
		urlProduct=new HashSet<String>();
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<String> getUrlProduct() {
		return urlProduct;
	}
	public void setUrlProduct(Set<String> urlProduct) {
		this.urlProduct = urlProduct;
	}




}
