package pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class StatScrap {

	//- Le temps d'exécution du scraping.
	//- Le nombre de produits remontés.
	//- Comment êtes vous certain de récupérer l'intégralité des produits du concurrent?

	protected  static AtomicLong executionTime=new AtomicLong();
	protected  static AtomicInteger numberPushedProducts=new AtomicInteger();

	public static int incrementCount() {
		return numberPushedProducts.incrementAndGet();
	}
	
	public static double incrementExecutionTime(long executionTimeAdd) {
		return executionTime.addAndGet(executionTimeAdd);
	}

	public static AtomicLong getExecutionTime() {
		return executionTime;
	}

	public static AtomicInteger getNumberPushedProducts() {
		return numberPushedProducts;
	}
	
	
}
