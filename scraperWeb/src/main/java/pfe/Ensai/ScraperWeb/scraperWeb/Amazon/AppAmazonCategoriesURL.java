package pfe.Ensai.ScraperWeb.scraperWeb.Amazon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class AppAmazonCategoriesURL {
	
	
	public static void scrapCategoriesAndWriteFiles()
	{
		//Scrap Categories URLs (à faire une fois, ensuite charger à partir de fichier).
		UrlsCategoryAmazonScraper urlsCategoryAmazonScraper=new UrlsCategoryAmazonScraper(true);
		urlsCategoryAmazonScraper.startScraper();	

		for (AmazonCategory category : urlsCategoryAmazonScraper.getCategories()) {
			category.writeURLAmazonCategory();
		}
	}
	
	
	public static List<AmazonCategory> loadCategoriesFromFiles(){
		
		List<AmazonCategory> categoriesLoadFromFiles=new ArrayList<AmazonCategory>();		
		File[] files = new File(AmazonCategory.DIRECTORY_URL_CATEGORY).listFiles();
		String urlCategory;
		String nameCategory;
		for (File file : files) {
			if (file.isFile()) {
				BufferedReader reader;
				try {
					reader = new BufferedReader(new FileReader(file));
					while (reader.ready()) {
						urlCategory=reader.readLine();
						nameCategory=file.getName().replace(".txt","");
						categoriesLoadFromFiles.add(new AmazonCategory(urlCategory, nameCategory));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}

		return categoriesLoadFromFiles;
		
	}

}
