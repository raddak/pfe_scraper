package pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import pfe.Ensai.ScraperWeb.scraperWeb.EanFind.EanFindScraper;
import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;

import com.google.gson.Gson;

public class ProducerProduitSynchrone implements Runnable {
	protected List<String> URLS; 
	protected IScraperProduct scraperProduct;
	protected boolean scrapEan;
	protected String queue;

	public ProducerProduitSynchrone(List<String> URLS, IScraperProduct scraperProduct,String queue,boolean scrapEan){
		this.URLS=URLS;
		this.scraperProduct=scraperProduct;
		this.scrapEan=scrapEan;
		this.queue=queue;
	}

	public void run() {
		try {

			//Execution Time
			long startTime = System.currentTimeMillis();

			// Create a ConnectionFactory
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("failover://(tcp://54.217.148.232:61616)?initialReconnectDelay=2000&maxReconnectAttempts=10");
			
			// Create a Connection
			Connection connection = connectionFactory.createConnection();
			connection.start();
			// Create a Session
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			
			// Create the destination (Topic or Queue)
			Destination destination = session.createQueue(queue);
			
			// Create a MessageProducer from the Session to the Topic or Queue
			MessageProducer producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);


			// Create a messages
			boolean succes;
			EanFindScraper eanFindScraper=new EanFindScraper();
			List<String> tempStringList=new ArrayList<String>();
			Gson gson = new Gson();
			
			for (String string : URLS) {
				System.out.println(string+" \n: Start Scraping");
				ProduitD productMessage;
				productMessage=scraperProduct.startScraper(string);
				succes=productMessage!=null;
				System.out.println(string+" \n: End Scraping\n");
				eanFindScraper.setNumberOfTries(0);
				if (succes) {
					if (this.scrapEan) {
						tempStringList.clear();
						if (productMessage.getNom()!=null) {
							tempStringList.addAll(Arrays.asList(productMessage.getNom().split("\\s+")));
							eanFindScraper.setParameters(productMessage,tempStringList);

							if (eanFindScraper.startScraper()) {
								productMessage.setEAN(eanFindScraper.getFoundEan());
							}
							else {
							//ean not found
							}
						}
					
					}					

					String text = gson.toJson(productMessage).toString()/* + Thread.currentThread().getName() + " : " + this.hashCode()*/;
					TextMessage message = session.createTextMessage(text);

					// Tell the producer to send the message
					System.out.println("Sent message: "+ message.hashCode() + " : " + Thread.currentThread().getName());
					producer.send(message);

				}
				else {
					//Failed Product Url
					System.out.println("fail");
				}
			}

			 //Clean up
			session.close();
			connection.close();

			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			StatScrap.incrementExecutionTime(elapsedTime);
		}
		catch (Exception e) {
			System.err.println("Caught: " + e);
			e.printStackTrace();
		}
	}

}
