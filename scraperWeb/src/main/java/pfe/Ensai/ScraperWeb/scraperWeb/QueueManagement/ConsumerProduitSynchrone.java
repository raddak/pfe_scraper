package pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import pfe.Ensai.ScraperWeb.scraperWeb.Cassandra.CassandraConnector;

public class ConsumerProduitSynchrone implements Runnable, ExceptionListener{

	final CassandraConnector client = new CassandraConnector();
	private Connection connection;
	private Session session;

	public ConsumerProduitSynchrone(){
		client.close();
	}

	protected void finalize()throws Throwable{
		super.finalize();

		// Clean up
		client.close();
		session.close();
		connection.close();
	}


	public void run() {
		try {
			
			client.connect(CassandraConnector.IP_ADDRESS, CassandraConnector.PORT);
			System.out.println("Connecting to IP Address " + CassandraConnector.IP_ADDRESS + ":" + CassandraConnector.PORT + "...");

			// Create a ConnectionFactory
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://54.74.21.141:61616");

			// Create a Connection
			Connection connection = connectionFactory.createConnection();
			connection.start();

			connection.setExceptionListener(this);

			// Create a Session
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Create the destination (Topic or Queue)
			Destination destination = session.createQueue("TEST.FOO");

			// Create a MessageConsumer from the Session to the Topic or Queue
			MessageConsumer consumer = session.createConsumer(destination);

			// Wait for a message
			Message message = consumer.receive(1000);
			if (message instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) message;
				String text = textMessage.getText();
				System.out.println("Received: " + text);
				//Perform operations on database Here
			} else {
				System.out.println("Received: " + message);
			}

			consumer.close();
			session.close();
			connection.close();
		} catch (Exception e) {
			System.out.println("Caught: " + e);
			e.printStackTrace();
		}
	}


	public synchronized void onException(JMSException ex) {
		System.out.println("JMS Exception occured.  Shutting down client.");
	}
}
