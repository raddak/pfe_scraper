package pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import pfe.Ensai.ScraperWeb.scraperWeb.Amazon.GenerateCsvAmazon;
import pfe.Ensai.ScraperWeb.scraperWeb.Cassandra.CassandraConnector;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitU;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.google.common.io.Files;
import com.google.gson.Gson;

public class ConsumerProduitAsynchrone implements Runnable, ExceptionListener{

	final CassandraConnector client = new CassandraConnector();
	private Connection connection;
	private Session session;
	private static Gson GSON_PARSER=new Gson();
	private Mapper<ProduitD> mapperD;
	private Mapper<ProduitU> mapperU;;
	private GenerateCsvAmazon generateCsvAmazon=new GenerateCsvAmazon("data.csv",true);

	

	protected  Set<String> tempCategory=new HashSet<String>();
	protected String queue;
	
	
	public ConsumerProduitAsynchrone(String queue){
		this.queue=queue;
	}

	protected void finalize()throws Throwable{
		super.finalize();

		// Clean up
		client.close();
		session.close();
		connection.close();
	}

	private class MyMessageListener implements MessageListener {
		public void onMessage(Message message) {
			if(message instanceof TextMessage) {
				try {					
					System.out.println("Received {} "+ ((TextMessage) message).getText());

					//Perform operations here (push to Cassandra)
					
					try {
						ProduitD productToAdd=GSON_PARSER.fromJson(((TextMessage) message).getText(),ProduitD.class);						
						mapperD.save(productToAdd);
						StatScrap.incrementCount();
						
						ProduitU productToAdd2=GSON_PARSER.fromJson(((TextMessage) message).getText(),ProduitU.class);		
						mapperU.save(productToAdd2);
						StatScrap.incrementCount();

						System.out.println("Push to cassandra Products : sucess");
						
						//Write CSV
						//generateCsvAmazon.writeProduct(productToAdd2);
						
						//category matching---> TO DO next
						
						
						if (tempCategory.add(productToAdd.getCategorie())) {
							try {
																
								Files.append(productToAdd.getCategorie()+"\n", new File("./categoryMatching/"+productToAdd.getSite()+".txt"),StandardCharsets.ISO_8859_1);
							} catch (IOException e) {
								e.printStackTrace();
							}
							
						}
						
					
					} catch (Exception e) {
						System.err.println("Failed to parse, "+e);
					}
					
					

				} catch (JMSException e) {
					System.err.println("Can't extract text from received message"+e);
				}
			}
			else
				System.out.println("Unexpected non-text message received.");
		}           
	}

	public void run() {
		try {
			
			client.connect(CassandraConnector.IP_ADDRESS, CassandraConnector.PORT);
			System.out.println("Connecting to IP Address " + CassandraConnector.IP_ADDRESS + ":" + CassandraConnector.PORT + "...");
		
			// Create a ConnectionFactory
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://54.217.148.232:61616");

			// Create a Connection
			connection = connectionFactory.createConnection();
			connection.start();

			connection.setExceptionListener(this);

			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Create the destination (Topic or Queue)
			Destination destination = session.createQueue(queue);

			mapperD=new MappingManager(client.getSession()).mapper(ProduitD.class);
			mapperU=new MappingManager(client.getSession()).mapper(ProduitU.class);
			
			MessageConsumer consumer = session.createConsumer(destination);
			consumer.setMessageListener(new MyMessageListener());

			try{ Thread.sleep(1000); } catch(InterruptedException e) {}

		} catch(JMSException ex) {
			ex.printStackTrace();
		}


	}

	public synchronized void onException(JMSException ex) {
		System.out.println("JMS Exception occured.  Shutting down client.");
	}		
}


