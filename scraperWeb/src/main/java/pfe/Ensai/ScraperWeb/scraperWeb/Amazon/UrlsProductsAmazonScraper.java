package pfe.Ensai.ScraperWeb.scraperWeb.Amazon;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraper;

public class UrlsProductsAmazonScraper implements IScraper{

	private AmazonCategory category;
	private Document doc;
	
	public UrlsProductsAmazonScraper(AmazonCategory category){
		this.category=category;
	}


	public boolean startScraper(int maxPage){
		
		int j=0;
		boolean newPage;
		boolean lastPage;
		
		if (maxPage==-1) {
			maxPage=Integer.MAX_VALUE;
		}

		try {
			doc=Jsoup.connect(category.getUrl()).timeout(10000).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}

		newPage=doc.getElementById("pagnNextLink")!=null && !doc.getElementById("pagnNextLink").attr("href").equals("");
		lastPage=false;

		j=0;

		while(j<=maxPage && (newPage||lastPage)){	

			System.out.println("*********"+j+"********");
			Elements productsElements=doc.select("li[id]");	
			for (Element element : productsElements) {

				Elements tempElements=element.select("[href]");
				if(!tempElements.isEmpty()) {
					System.out.println(tempElements.first().attr("href"));
					category.getUrlProduct().add(tempElements.first().attr("href"));
				}

			}



			newPage=doc.getElementById("pagnNextLink")!=null && !doc.getElementById("pagnNextLink").attr("href").equals("");

			if (lastPage) {
				lastPage=false;
			}
			else if (!newPage) {
				lastPage=true;
			}

			if (newPage) {
				String pageNextLink="http://amazon.fr"+doc.getElementById("pagnNextLink").attr("href");
				try {
					doc=Jsoup.connect(pageNextLink).timeout(10000).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}

			j++;
		}

			category.writeURLProducts();
			
			return true;
	}


	public boolean startScraper() {
		boolean newPage;
		boolean lastPage;
		

		try {
			doc=Jsoup.connect(category.getUrl()).timeout(10000).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}

		newPage=doc.getElementById("pagnNextLink")!=null && !doc.getElementById("pagnNextLink").attr("href").equals("");
		lastPage=false;

		while((newPage||lastPage)){	

			Elements productsElements=doc.select("li[id]");	
			for (Element element : productsElements) {

				Elements tempElements=element.select("[href]");
				if(!tempElements.isEmpty()) {
					System.out.println(tempElements.first().attr("href"));
					category.getUrlProduct().add(tempElements.first().attr("href"));
				}

			}



			newPage=doc.getElementById("pagnNextLink")!=null && !doc.getElementById("pagnNextLink").attr("href").equals("");

			if (lastPage) {
				lastPage=false;
			}
			else if (!newPage) {
				lastPage=true;
			}

			if (newPage) {
				String pageNextLink="http://amazon.fr"+doc.getElementById("pagnNextLink").attr("href");
				try {
					doc=Jsoup.connect(pageNextLink).timeout(10000).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}

		}

			category.writeURLProducts();
			
			return true;
	}
}
