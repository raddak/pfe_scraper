package pfe.Ensai.ScraperWeb.scraperWeb.EanFind;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraper;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;

public class EanFindScraper implements IScraper{

	protected Document doc;
	protected final static String URL_DEFAULT="https://www.eanfind.fr/";
	protected List<String> searchKeywords;
	protected final static String URL_QUERY_BETWEEN_WORDS="%20";
	protected ProduitD product;
	protected String foundEan="";
	protected int numberOfTries=0;
	protected static final Set<String> COLORS=initColors();
	private static final String DIRECTORY_COLOR = "./colors/";

	public EanFindScraper(ProduitD product,List<String> searchKeywords){
		this.searchKeywords=searchKeywords;
		this.product=product;
	}


	public EanFindScraper() {
	}


	private static Set<String> initColors(){
		Set<String> colors=new HashSet<String>();
		File[] files1 = new File(DIRECTORY_COLOR).listFiles();

		for (File file : files1) {
			if (file.isFile()) {
				BufferedReader reader;
				try {
					reader = new BufferedReader(new FileReader(file));
					String word;				
					while (reader.ready()) {
						word=reader.readLine();
						colors.add(word.toLowerCase().trim());
					}
					reader.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}


		return colors;
	}

	public int getNumberOfTries() {
		return numberOfTries;
	}


	public void setNumberOfTries(int numberOfTries) {
		this.numberOfTries = numberOfTries;
	}


	public String getFoundEan() {
		return foundEan;
	}


	public static void main(String[] args) {


		//		List<String> group=new ArrayList<String>();
		//		System.out.println(getCombinationsFor(group, 2));

		List<String> listString= new ArrayList<String>();
		listString.add("ps4");
		//EanFindScraper eanFindScraper=new EanFindScraper(null, listString);
		//eanFindScraper.startScraper();
		//eanFindScraper.changeKeywordsKeepThreeWordsMax();;
		//System.out.println(eanFindScraper.searchKeywords);
		//System.out.println(COLORS);

	}

	public boolean startScraper() {
		String url=URL_DEFAULT+"chercher/";
		String temp;
		for (int i = 0; i < searchKeywords.size(); i++) {
			temp=searchKeywords.get(i).replace("%", "");
			if (temp!="") {
				url+=temp;
				if (i<searchKeywords.size()-1) {
					url+=URL_QUERY_BETWEEN_WORDS;
				}
			}
			
		}

		try {
			doc=Jsoup.connect(url).timeout(10000).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
		} catch (Exception e) {

			//e.printStackTrace();
			return false;
		}

		boolean result=false;
		if (isResultFound()) {
			if (isReturnOneResult()) {
				//Check Price
				foundEan=doc.getElementById("eanProduct").attr("value");
				result=true;
			}
			else {
				if (takeFirstProductMatchingCriteria()) {	
					foundEan=doc.getElementById("eanProduct").attr("value");
					result=true;

				}else {
				}

			}
		}
		else {
			//try new strategie

			if (numberOfTries==0) {
				changeKeywordsRemoveNumberAndWordsSizeTwo();
				numberOfTries++;
				if (startScraper()) {
					return true;
				}
			}
			if (numberOfTries==1) {
				changeKeywordsRemoveColor();
				numberOfTries++;
				if (startScraper()) {
					return true;
				}
			}
			
			if (numberOfTries==2) {
				changeKeywordsKeepHalfOfWords();
				numberOfTries++;
				if (startScraper()) {
					return true;
				}
			}
					
			
			if (numberOfTries==3) {
				numberOfTries++;
			}
		}
		return result;
	}

	private boolean isResultFound(){

		boolean result=true;
		Element ratingMeanElement=doc.getElementById("main-content");
		Elements tempElements;
		if (ratingMeanElement!=null) {
			tempElements=ratingMeanElement.select("p[class^=no-result]");
			if (!tempElements.isEmpty()) {
				result=false;
			}
		}


		return result;

	}

	private boolean isReturnOneResult(){

		boolean result=false;
		Element ratingMeanElement=doc.getElementById("eanProduct");

		if (ratingMeanElement!=null) {			
			result=true;
		}

		return result;

	}


	private boolean takeFirstProductMatchingCriteria(){
		boolean result=false;

		//Criteria price epsilon=50%
		// à revoir
		Double priceTempString=this.product.getPrixVente();
		if (priceTempString==null) {
			Elements productsElements=doc.select("ul[class^=search-result]");

			productsElements=productsElements.select("li");

			if (productsElements!=null) {

				productsElements=productsElements.select("li");
				for (Element element : productsElements) {

					Elements tempElements=element.select("img[title]");
					Elements tempElements2=element.select("span[class=price]");
					if(!tempElements.isEmpty()) {


						if (!tempElements2.isEmpty()) {
							if (!element.select("[href]").isEmpty()) {
								String pageNextLink=element.select("[href]").first().attr("href");
								try {
									doc=Jsoup.connect(pageNextLink).timeout(10000).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
									return result=true;
								} catch (IOException e) {
									e.printStackTrace();
								}

							}
						}
					}
				}
			}
		}


		int price=priceTempString.intValue();
		/*try {
			price=Integer.parseInt(priceTempString);

		} catch (Exception e) {
			//NumberformatException
			return result;
		}*/

		int priceTemp;



		doc.getElementById("content");

		Elements productsElements=doc.select("ul[class^=search-result]");


		if (productsElements!=null) {

			productsElements=productsElements.select("li");
			for (Element element : productsElements) {

				Elements tempElements=element.select("img[title]");
				Elements tempElements2=element.select("span[class=price]");
				if(!tempElements.isEmpty()) {


					if (!tempElements2.isEmpty()) {
						String priceFull=tempElements2.first().text();
						priceFull=priceFull.substring(0, priceFull.indexOf(","));
						try {
							priceTemp=Integer.parseInt(priceFull);

							if (priceTemp>=price/2 && priceTemp<=price*2) {

								if (!element.select("[href]").isEmpty()) {
									String pageNextLink=element.select("[href]").first().attr("href");


									try {
										doc=Jsoup.connect(pageNextLink).timeout(10000).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
										result=true;
									} catch (IOException e) {
										e.printStackTrace();
									}

									break;
								}
							}

						} catch (Exception e) {
							//numberformatexception
							return result;
						}
					}
					else {
						//No one match
						//get back take first result or no result
					}


				}
			}
		}

		return result;

	}

	private void changeKeywordsRemoveNumberAndWordsSizeTwo(){
		//retirer les chifres
		List<String> searchKeywordsWithoutNumber=new ArrayList<String>();
		for (String string : searchKeywords) {
			if (!string.matches(".*[1-9].*") && string.length()>2) {
				searchKeywordsWithoutNumber.add(string);
			}
		}

		searchKeywords=searchKeywordsWithoutNumber;

	}

	private void changeKeywordsRemoveColor(){
		//retirer les chifres
		List<String> searchKeywordsWithoutColor=new ArrayList<String>();
		for (String string : searchKeywords) {
			if (!COLORS.contains(string.toLowerCase().trim())) {
				searchKeywordsWithoutColor.add(string);
			}
		}

		searchKeywords=searchKeywordsWithoutColor;

	}

	private void changeKeywordsKeepHalfOfWords(){
		//premier mots
		List<String> searchKeywordscKeepHalfOfWords=new ArrayList<String>();
		for (int i = 0; i < (searchKeywords.size()/2); i++) {
			searchKeywordscKeepHalfOfWords.add(searchKeywords.get(i));
		}

		searchKeywords=searchKeywordscKeepHalfOfWords;

	}



	public void setParameters(ProduitD product,List<String> searchKeywords){
		this.searchKeywords=searchKeywords;
		this.product=product;
	}



	public static List<List<String>> getCombinationsFor(List<String> group, int subsetSize) {
		List<List<String>> resultingCombinations = new ArrayList<List<String>> ();
		int totalSize=group.size();
		if (subsetSize == 0) {
			emptySet(resultingCombinations);
		} else if (subsetSize <= totalSize) {
			List<String> remainingElements = new ArrayList<String> (group);
			String X = popLast(remainingElements);

			List<List<String>> combinationsExclusiveX = getCombinationsFor(remainingElements, subsetSize);
			List<List<String>> combinationsInclusiveX = getCombinationsFor(remainingElements, subsetSize-1);
			for (List<String> combination : combinationsInclusiveX) {
				combination.add(X);
			}
			resultingCombinations.addAll(combinationsExclusiveX);
			resultingCombinations.addAll(combinationsInclusiveX);
		}
		return resultingCombinations;
	}

	private static void emptySet(List<List<String>> resultingCombinations) {
		resultingCombinations.add(new ArrayList<String>());
	}

	private static String popLast(List<String> elementsExclusiveX) {
		return elementsExclusiveX.remove(elementsExclusiveX.size()-1);
	}
}
