package pfe.Ensai.ScraperWeb.scraperWeb.Amazon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import pfe.Ensai.ScraperWeb.scraperWeb.EanFind.EanFindScraper;
import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitU;
import pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement.StatScrap;

import com.google.gson.Gson;

public class AppAmazonProduct 
{

	public class TestConsumer implements Runnable, ExceptionListener{

		private Connection connection;
		private Session session;
		private Gson GSON_PARSER=new Gson();
		private GenerateCsvAmazon generateCsvAmazon=new GenerateCsvAmazon("Amazon23.csv",true);
		protected String queue;


		public TestConsumer(String queue){
			this.queue=queue;
		}


		private class MyMessageListener implements MessageListener {
			public void onMessage(Message message) {
				if(message instanceof TextMessage) {
					try {					
						System.out.println("Received {} "+ ((TextMessage) message).getText());


						try {

							ProduitU productToAdd2=GSON_PARSER.fromJson(((TextMessage) message).getText(),ProduitU.class);		
							generateCsvAmazon.writeProduct(productToAdd2);
							System.out.println("Push to csv ProductsU : sucess");
							StatScrap.incrementCount();


						} catch (Exception e) {
							System.err.println("Failed to parse, "+e);
						}



					} catch (JMSException e) {
						System.err.println("Can't extract text from received message"+e);
					}
				}
				else
					System.out.println("Unexpected non-text message received.");
			}           
		}

		public void run() {
			try {

				// Create a ConnectionFactory
				ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

				// Create a Connection
				connection = connectionFactory.createConnection();
				connection.start();

				connection.setExceptionListener(this);

				session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

				// Create the destination (Topic or Queue)
				Destination destination = session.createQueue(queue);

				MessageConsumer consumer = session.createConsumer(destination);
				consumer.setMessageListener(new MyMessageListener());

				try{ Thread.sleep(1000); } catch(InterruptedException e) {}

			} catch(JMSException ex) {
				ex.printStackTrace();
			}


		}

		public synchronized void onException(JMSException ex) {
			System.out.println("JMS Exception occured.  Shutting down client.");
		}		
	}
	public class TestProducer implements Runnable {
		protected List<String> URLS; 
		protected IScraperProduct scraperProduct;
		protected boolean scrapEan;
		protected String queue;

		public TestProducer(List<String> URLS, IScraperProduct scraperProduct,String queue,boolean scrapEan){
			this.URLS=URLS;
			this.scraperProduct=scraperProduct;
			this.scrapEan=scrapEan;
			this.queue=queue;
		}

		public void run() {
			try {

				//Execution Time
				long startTime = System.currentTimeMillis();

				// Create a ConnectionFactory
				ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

				// Create a Connection
				Connection connection = connectionFactory.createConnection();
				connection.start();
				// Create a Session
				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

				// Create the destination (Topic or Queue)
				Destination destination = session.createQueue(queue);

				// Create a MessageProducer from the Session to the Topic or Queue
				MessageProducer producer = session.createProducer(destination);
				producer.setDeliveryMode(DeliveryMode.PERSISTENT);


				// Create a messages
				boolean succes;
				EanFindScraper eanFindScraper=new EanFindScraper();
				List<String> tempStringList=new ArrayList<String>();
				Gson gson = new Gson();

				for (String string : URLS) {
					System.out.println(string+" \n: Start Scraping");
					ProduitD productMessage;
					productMessage=scraperProduct.startScraper(string);
					succes=productMessage!=null;
					System.out.println(string+" \n: End Scraping\n");
					eanFindScraper.setNumberOfTries(0);
					if (succes) {
						if (this.scrapEan) {
							tempStringList.clear();
							tempStringList.addAll(Arrays.asList(productMessage.getNom().split("\\s+")));
							eanFindScraper.setParameters(productMessage,tempStringList);
							System.out.println(tempStringList);

							if (eanFindScraper.startScraper()) {
								System.out.println("/*********"+eanFindScraper.getFoundEan()+"**********/");
								productMessage.setEAN(eanFindScraper.getFoundEan());
							}
							else {
								System.out.println("/********ean not found**********/");
							}
						}					

						String text = gson.toJson(productMessage).toString()/* + Thread.currentThread().getName() + " : " + this.hashCode()*/;
						TextMessage message = session.createTextMessage(text);

						// Tell the producer to send the message
						System.out.println("Sent message: "+ message.hashCode() + " : " + Thread.currentThread().getName());
						producer.send(message);

					}
					else {
						//Failed Product Url
					}
				}

				// Clean up
				session.close();
				connection.close();

				long stopTime = System.currentTimeMillis();
				long elapsedTime = stopTime - startTime;
				System.out.println("elapsedTime: "+elapsedTime);
				StatScrap.incrementExecutionTime(elapsedTime);
			}
			catch (Exception e) {
				System.out.println("Caught: " + e);
				e.printStackTrace();
			}
		}

	}



	public static void main( String[] args )
	{
		String lastfile;
		AppAmazonProduct testApp=new AppAmazonProduct();

		File[] files1 = new File(AmazonCategory.DIRECTORY_URL_PRODUCTS).listFiles();
		List<List<String>> tempStringList=new ArrayList<List<String>>();
		List<String> temp=new ArrayList<String>();
		for (File file : files1) {
			if (file.isFile()) {
				BufferedReader reader;
				try {
					reader = new BufferedReader(new FileReader(file));
					temp=new ArrayList<String>();	
					temp.add(file.getName().replace(".txt", "").replace("URL", "data").concat(".csv"));
					while (reader.ready()) {
						temp.add(reader.readLine());
					} 
					reader.close();		
					tempStringList.add(temp);
					System.out.println(file.getName());
				}
				catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
		
		

		//MultiThreading avec executor (share load of URLS):
		int nThreads = Runtime.getRuntime().availableProcessors();
		System.out.println(nThreads);
		
		
		Thread brokerThread = new Thread(testApp.new TestConsumer("AmazonQueue"));
		brokerThread.setDaemon(false);
		brokerThread.start();

		ExecutorService executor;
		for (int i=11;i<tempStringList.size();i++) {
			executor=Executors.newFixedThreadPool(nThreads*5);
			lastfile=tempStringList.get(i).get(0);
			System.out.println(lastfile);
			List<List<String>> subLists=MyPartition.partition(tempStringList.get(i), nThreads*5);
			IScraperProduct amazonProductScraper=new AmazonProductScraper();
			for (int j=0;j<subLists.size();j++) {
				Runnable worker = testApp.new TestProducer(subLists.get(j),amazonProductScraper,"AmazonQueue",true);
				executor.execute(worker);
			}
			executor.shutdown();
			while (!executor.isTerminated()) {
			}

			System.out.println("Finished all threads");
		}
		
		
		//Stats
		System.out.println("Temps d'éxécution Total"+StatScrap.getExecutionTime());
		System.out.println("Nombre de produit push sur cassandra à partir ce consumer"+StatScrap.getNumberPushedProducts());




		//		//Categories URLs
		//		//AppAmazonCategoriesURL.scrapCategoriesAndWriteFiles();;
		//
		//		//UrlProductsPerCategory URLs
		//		//AppAmazonProductsURL.scrapProductsPerCateogryAndWriteFiles(AppAmazonCategoriesURL.loadCategoriesFromFiles(), -1, -1);
		//
		//		boolean succes;
		//
		//		//Scrap UrlsProducts
		//		//List<AmazonProduct> amazonProducts=new ArrayList<AmazonProduct>();		
		//		File[] files1 = new File(AmazonCategory.DIRECTORY_URL_PRODUCTS).listFiles();
		//		GenerateCsvAmazon generateCsvAmazon;
		//		EanFindScraper eanFindScraper=new EanFindScraper();
		//		List<String> tempStringList=new ArrayList<String>();
		//		for (File file : files1) {
		//			if (file.isFile()) {
		//				generateCsvAmazon=new GenerateCsvAmazon(file.getName().replace(".txt", "").replace("URL", "data").concat(".csv"),false);
		//				System.out.println(file.getName());
		//				BufferedReader reader;
		//				try {
		//					reader = new BufferedReader(new FileReader(file));
		//					AmazonProductScraper amazonProduct=new AmazonProductScraper();
		//					ProduitD produitTemp;
		//					String url;				
		//					while (reader.ready()) {
		//						url=reader.readLine();
		//						produitTemp=amazonProduct.startScraper(url);	
		//						System.out.println(url+" \n: Start Scraping");
		//						succes=produitTemp!=null;
		//						System.out.println(url+" \n: End Scraping\n");
		//						//System.out.println(amazonProduct);
		//						eanFindScraper.setNumberOfTries(0);
		//						if (succes) {
		//							tempStringList.clear();
		//							tempStringList.addAll(Arrays.asList(produitTemp.getNom().split("\\s+")));
		//							eanFindScraper.setParameters(produitTemp,tempStringList);
		//							System.out.println(tempStringList);
		//
		//							if (eanFindScraper.startScraper()) {
		//								System.out.println("/*********"+eanFindScraper.getFoundEan()+"**********/");
		//							}
		//							else {
		//								System.out.println("/********ean not found**********/");
		//							}
		//
		//							generateCsvAmazon.writeProduct(produitTemp);
		//						}
		//						else {
		//							//Failed Product Url
		//						}
		//					}
		//					reader.close();
		//				} catch (Exception e) {
		//					e.printStackTrace();
		//				}
		//
		//			}
		//		}

	}


	public static class MyPartition {


		/**
		 * Returns consecutive {@linkplain List#subList(int, int) sublists} of a list,
		 * each of the same size (the final list may be smaller). For example,
		 * partitioning a list containing {@code [a, b, c, d, e]} with a partition
		 * size of 3 yields {@code [[a, b, c], [d, e]]} -- an outer list containing
		 * two inner lists of three and two elements, all in the original order.
		 *
		 * <p>The outer list is unmodifiable, but reflects the latest state of the
		 * source list. The inner lists are sublist views of the original list,
		 * produced on demand using {@link List#subList(int, int)}, and are subject
		 * to all the usual caveats about modification as explained in that API.
		 *
		 * * Adapted from http://code.google.com/p/google-collections/ 
		 *
		 * @param list the list to return consecutive sublists of
		 * @param size the desired size of each sublist (the last may be
		 *     smaller)
		 * @return a list of consecutive sublists
		 * @throws IllegalArgumentException if {@code partitionSize} is nonpositive
		 * 

		 */



		public static <T> List<List<T>> partition(List<T> list, int size) {

			if (list == null)
				throw new NullPointerException("'list' must not be null");
			if (!(size > 0))
				throw new IllegalArgumentException("'size' must be greater than 0");

			return new Partition<T>(list, size);
		}

		private static class Partition<T> extends AbstractList<List<T>> {

			final List<T> list;
			final int size;

			Partition(List<T> list, int size) {
				this.list = list;
				this.size = size;
			}

			@Override
			public List<T> get(int index) {
				int listSize = size();
				if (listSize < 0)
					throw new IllegalArgumentException("negative size: " + listSize);
				if (index < 0)
					throw new IndexOutOfBoundsException("index " + index + " must not be negative");
				if (index >= listSize)
					throw new IndexOutOfBoundsException("index " + index + " must be less than size " + listSize);
				int start = index * size;
				int end = Math.min(start + size, list.size());
				return list.subList(start, end);
			}

			@Override
			public int size() {
				return (list.size() + size - 1) / size;
			}

			@Override
			public boolean isEmpty() {
				return list.isEmpty();
			}
		}


	} 
}
