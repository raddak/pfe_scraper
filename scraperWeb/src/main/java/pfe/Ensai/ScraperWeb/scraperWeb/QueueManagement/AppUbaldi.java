package pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;
import pfe.Ensai.ScraperWeb.scraperWeb.Ubaldi.ScrapingUbaldi;

public class AppUbaldi extends AppQueueMulti {

	public static void main(String[] args) throws IOException {
		
		List<String> tempStringList=new ArrayList<String>();
	
		
		
		//MultiThreading avec executor (share load of URLS):
		int nThreads = Runtime.getRuntime().availableProcessors();
	    System.out.println(nThreads);

		thread(new ConsumerProduitAsynchrone("UbaldiQueue"), false);
		
		ExecutorService executor = Executors.newFixedThreadPool(15);
		
		IScraperProduct scrapProducts=new ScrapingUbaldi();
		ArrayList<String> listeProd = new ArrayList<String>();
		listeProd=((ScrapingUbaldi)scrapProducts).getLiensProduits();
		System.out.println(listeProd.size());
		tempStringList=(listeProd);
		
		List<List<String>> subLists=MyPartition.partition(tempStringList, listeProd.size()/10);
		for (List<String> list : subLists) {
            Runnable worker = new ProducerProduitSynchrone(list,scrapProducts,"UbaldiQueue",false);
            executor.execute(worker);
		}
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
        
		
	}

}
