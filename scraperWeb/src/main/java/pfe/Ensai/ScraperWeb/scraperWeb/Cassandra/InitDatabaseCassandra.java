package pfe.Ensai.ScraperWeb.scraperWeb.Cassandra;


public class InitDatabaseCassandra {
	
	public static void main(final String[] args)
	{
		final CassandraConnector client = new CassandraConnector();
		System.out.println("Connecting to IP Address " + CassandraConnector.IP_ADDRESS + ":" + CassandraConnector.PORT + "...");
		client.connect(CassandraConnector.IP_ADDRESS, CassandraConnector.PORT);
		
		final String createKeySpace ="CREATE KEYSPACE IF NOT EXISTS PFE_KEYSPACE WITH replication "
				+ "= {'class':'SimpleStrategy', 'replication_factor':1};";
		try {
			client.getSession().execute(createKeySpace);
		} catch (Exception e) {
			e.printStackTrace();
		}



		try {

			final String deleteDatabase="DROP TABLE IF EXISTS PFE_KEYSPACE.productsU";
			client.getSession().execute(deleteDatabase);		
			final String deleteDatabase2="DROP TABLE IF EXISTS PFE_KEYSPACE.productsD";
			client.getSession().execute(deleteDatabase2);		
		} catch (Exception e) {
			e.printStackTrace();
		}


		try {
			final String createDatabase =""
					+ "CREATE TABLE IF NOT EXISTS PFE_KEYSPACE.productsD "
					+ "("
					+ "reference varchar,"
					+ "dateScrap timestamp, "
					+ "site varchar, "
					+ "URL varchar, "
					+ "nom varchar, "
					+ "marque varchar, "
					+ "description varchar, "
					+ "prixVente double, "
					+ "EAN text, "
					+ "categorie varchar, "
					+ "sousCategorie varchar, "
					+ "prixSolde double, "
					+ "otherPriceNew double, "
					+ "otherPriceUsed double,"
					+ "classement varchar, "
					+ "ratingMean varchar, "
					+ "nbComment varchar, "
					+ "isDisponible boolean, "
					+ "isSoldbyAmazon boolean, "
					+ "asinRecommandationProduct set<text>, "
					+ "keyWords set<text>,"					
					+ "PRIMARY KEY ((reference,site),dateScrap)"
					+ ") WITH CLUSTERING ORDER BY (dateScrap DESC)";
			
			client.getSession().execute(createDatabase);
			
			final String createDatabase2 =""
					+ "CREATE TABLE IF NOT EXISTS PFE_KEYSPACE.productsU "
					+ "("
					+ "reference varchar,"
					+ "site varchar, "
					+ "URL varchar, "
					+ "nom varchar, "
					+ "marque varchar, "
					+ "description varchar, "
					+ "prixVente double, "
					+ "EAN text, "
					+ "categorie varchar, "
					+ "sousCategorie varchar, "
					+ "prixSolde double, "
					+ "otherPriceNew double, "
					+ "otherPriceUsed double,"
					+ "classement varchar, "
					+ "ratingMean varchar, "
					+ "nbComment varchar, "
					+ "isDisponible boolean, "
					+ "isSoldbyAmazon boolean, "
					+ "asinRecommandationProduct set<text>, "
					+ "keyWords set<text>,"					
					+ "PRIMARY KEY (reference,site)"
					+ ")";
			client.getSession().execute(createDatabase2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		client.close();
	}

}
