package pfe.Ensai.ScraperWeb.scraperWeb.Amazon;

import java.util.HashSet;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraper;

public class UrlsCategoryAmazonScraper implements IScraper{

	private static final String URL_DEFAULT="http://www.amazon.fr/s/ref=nb_sb_noss/276-1716729-2285013?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&url=search-alias%3Dbaby&field-keywords=";
	private Set<String> notACategory;
	private String allNotIncluded="Toute";
	private Document doc;
	private Set<AmazonCategory> categories;
	//process become slower with this option
	private boolean withUnavailableProducts;

	public UrlsCategoryAmazonScraper(boolean withUnavailableProducts){
		initNotACategory();		
		this.withUnavailableProducts=withUnavailableProducts;
	}

	private void initNotACategory(){		
		notACategory=new HashSet<String>();
		notACategory.add("Amazon Famille");
		notACategory.add("Liste de Naissance");
		notACategory.add("Guides d'achats et sélections");
		notACategory.add("Les indispensables de Bébé");
	}

	private boolean includeCategory(String nameCategory){

		return !(notACategory.contains(nameCategory) || nameCategory.startsWith(allNotIncluded));
	}

	public boolean startScraper(){		
		try {
			doc=Jsoup.connect(URL_DEFAULT).timeout(0).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}


		categories=new HashSet<AmazonCategory>();

		Element catgoriesElement=doc.getElementById("leftNav");
		Elements catgoriesElements=catgoriesElement.select("ul").select("li");

		String nameCategoryTemp="";
		String urlTemp="";
		for (Element element : catgoriesElements) {
			nameCategoryTemp=element.text();
			urlTemp="http://www.amazon.fr"+element.select("[href]").first().attr("href");
			if (includeCategory(nameCategoryTemp)) {
				
				if (withUnavailableProducts) {
					String urlTransform=showUnvailableProducts(urlTemp);
					if (!urlTransform.equals("")) {
						urlTemp=urlTransform;
					}
				}
				
				categories.add(new AmazonCategory(urlTemp,nameCategoryTemp));
				
			}

		}

		return true;

	}

	private String showUnvailableProducts(String url){
		
		try {
			doc=Jsoup.connect(url).timeout(0).userAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)").get();
		} catch (Exception e) {

			e.printStackTrace();
			return "";
		}
		
		Elements elementsTemp=doc.select("ul[id^=ref_]");
		String urlTransform="";
		if (!elementsTemp.isEmpty()) {
			
			elementsTemp=elementsTemp.select("img[alt*=Ajouter les articles non en stock]").parents();
			
			if (!elementsTemp.isEmpty()) {				
				urlTransform=elementsTemp.attr("href");
			}
			
		}
		if (!urlTransform.equals("")) {
			urlTransform="http://www.amazon.fr"+urlTransform;
		}
		return urlTransform;
	}

	public Set<String> getNotACategory() {
		return notACategory;
	}

	public void setNotACategory(Set<String> notACategory) {
		this.notACategory = notACategory;
	}

	public String getAllNotIncluded() {
		return allNotIncluded;
	}

	public void setAllNotIncluded(String allNotIncluded) {
		this.allNotIncluded = allNotIncluded;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}

	public Set<AmazonCategory> getCategories() {
		return categories;
	}

	public void setCategories(Set<AmazonCategory> categories) {
		this.categories = categories;
	}

	public static String getUrlDefault() {
		return URL_DEFAULT;
	}


}
