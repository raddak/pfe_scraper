package pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;
import pfe.Ensai.ScraperWeb.scraperWeb.RoseOuBleu.AppRoseOuBleu;

public class AppRoB extends AppQueueMulti{

	public static void main(String[] args) throws Exception {		

		List<String> tempStringList=new ArrayList<String>();
	
		
		
		//MultiThreading avec executor (share load of URLS):
		int nThreads = Runtime.getRuntime().availableProcessors();
	    System.out.println(nThreads);

		thread(new ConsumerProduitAsynchrone("RoBQueue"), false);
		
		ExecutorService executor = Executors.newFixedThreadPool(nThreads-1);
		
		
		IScraperProduct robProductScraper=new AppRoseOuBleu();
		tempStringList=AppRoseOuBleu.retourneLiensProduits();
		System.out.println("coucou je suis là");
		List<List<String>> subLists=MyPartition.partition(tempStringList, nThreads-1);
		for (List<String> list : subLists) {
            Runnable worker = new ProducerProduitSynchrone(list,robProductScraper,"RoBQueue",false);
            executor.execute(worker);
		}
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
        
		
	}

}

