package pfe.Ensai.ScraperWeb.scraperWeb.QueueManagement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pfe.Ensai.ScraperWeb.scraperWeb.Amazon.AmazonCategory;
import pfe.Ensai.ScraperWeb.scraperWeb.Amazon.AmazonProductScraper;
import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;

public class AppAmazon extends AppQueueMulti{

	public static void main(String arg[]) throws Exception {		
		List<String> tempStringList=new ArrayList<String>();

		Map<Integer, File> fileNames=new HashMap<Integer, File>();


		File[] files1 = new File("./test/").listFiles();
		File fileChosen=null;
		int i=1;
		for (File file : files1) {
			if (file.isFile()) {
				fileNames.put(i, file);
				i++;
			}
		}

		boolean again=true;

		do {

			Scanner user_input = new Scanner( System.in );
			Integer choice=null;
			System.out.println("***********");
			System.out.println("Enter the number associated to the file wanted (0 for all Files, -1 exit) :");
			System.out.println("0 : All");
			for (Integer integer : fileNames.keySet()) {

				System.out.println(integer+" : "+fileNames.get(integer).getName());

			}

			choice=user_input.nextInt();
			if (choice!=null && choice==0) {
				files1 = new File("./test/").listFiles();
				for (File file : files1) {
					if (file.isFile()) {
						BufferedReader reader;
						try {
							reader = new BufferedReader(new FileReader(file));
							while (reader.ready()) {
								tempStringList.add(reader.readLine());
							} 
							reader.close();					
						}
						catch (Exception e) {
							e.printStackTrace();
						}

					}
				}
				user_input.close();
				again=false;
			}
			else if (choice!=null && fileNames.containsKey(choice)) {
				fileChosen=fileNames.get(choice);
				user_input.close();
				again=false;
			}
			else if(choice!=null && choice==-1) {
				System.out.println("Exit.");
				user_input.close();
				return;
			}
			else {
				System.out.println("This file does not exist.;");
				again=true;
			}
			System.out.println("***********");

		} 
		while (again);


		if (fileChosen!=null) {
			BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(fileChosen));
				while (reader.ready()) {
					tempStringList.add(reader.readLine());
				} 
				reader.close();					
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}




		//MultiThreading avec executor (share load of URLS):
		int nThreads = Runtime.getRuntime().availableProcessors();
		System.out.println(nThreads);
		System.out.println(tempStringList.size());

		thread(new ConsumerProduitAsynchrone("AmazonQueue"), false);
		
		ExecutorService executor = Executors.newFixedThreadPool(15);

		List<List<String>> subLists=MyPartition.partition(tempStringList, tempStringList.size()/10);
		IScraperProduct amazonProductScraper=new AmazonProductScraper();

		//File dir = new File("./test/");
		//int k=1;
		for (List<String> list : subLists) {
			Runnable worker = new ProducerProduitSynchrone(list,amazonProductScraper,"AmazonQueue",true);
			executor.execute(worker);
		}


		executor.shutdown();
		while (!executor.isTerminated()) {
		}

		System.out.println("Finished all threads");

		//Stats
		System.out.println("Temps d'éxécution Total"+StatScrap.getExecutionTime());
		System.out.println("Nombre de produit push sur cassandra à partir ce consumer"+StatScrap.getNumberPushedProducts());


	}

}