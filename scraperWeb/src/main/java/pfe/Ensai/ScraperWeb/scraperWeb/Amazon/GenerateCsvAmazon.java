package pfe.Ensai.ScraperWeb.scraperWeb.Amazon;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitU;

public class GenerateCsvAmazon
{
	private final static String SEPAR_CHARAC=";";
	private String sFileName;

	private final static String FOLDER_DEFAULT="./generatedCSV/Amazon/";
	private final static String SFILENAME_DEFAULT="Amazontest.csv";
	private BufferedWriter bw;

	public GenerateCsvAmazon(boolean append){
		this.sFileName=SFILENAME_DEFAULT;
		if (!append) {
			generateCsvFileInit();
		}
	}

	public GenerateCsvAmazon(String sFileName, boolean append){
		this.sFileName=sFileName;
		if (!append) {
			generateCsvFileInit();
		}
	}

	private void generateCsvFileInit(){
		try
		{
			File dir = new File(FOLDER_DEFAULT);
			dir.mkdirs();
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FOLDER_DEFAULT+sFileName,false), "ISO-8859-1"));
			StringBuffer oneLine = new StringBuffer();
			
			oneLine.append("reference");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("site");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("URL");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("nom");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("marque");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("description");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("EAN");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("categorie");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("sousCategorie");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("prixVente");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("prixSolde");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("otherPriceNew");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("otherPriceUsed");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("classement");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("ratingMean");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("nbComment");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("isDisponible");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("isSoldbyAmazon");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("asinRecommandationProduct");
			oneLine.append(SEPAR_CHARAC);
			oneLine.append("keyWords");

			bw.write(oneLine.toString());
			bw.newLine();
			bw.flush();
			bw.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		} 
	}

	public void writeProduct(ProduitU product)
	{
		try
		{
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FOLDER_DEFAULT+sFileName,true), "ISO-8859-1"));
			StringBuffer oneLine = new StringBuffer();

			oneLine.append(product.getReference());
			oneLine.append(SEPAR_CHARAC);
			
			oneLine.append(product.getSite());
			oneLine.append(SEPAR_CHARAC);
			
			oneLine.append(product.getURL());
			oneLine.append(SEPAR_CHARAC);
			
			oneLine.append(product.getNom());
			oneLine.append(SEPAR_CHARAC);

			oneLine.append(product.getMarque());
			oneLine.append(SEPAR_CHARAC);
			
			String string=null;
			if (product.getDescription()!=null) {
				 string=product.getDescription().replaceAll(SEPAR_CHARAC,"/").replaceAll("/n", " ");
			}
			oneLine.append(string);
			oneLine.append(SEPAR_CHARAC);

			oneLine.append(product.getEAN());
			oneLine.append(SEPAR_CHARAC);
			

			oneLine.append(product.getCategorie());
			oneLine.append(SEPAR_CHARAC);
			
			oneLine.append(product.getSousCategorie());
			oneLine.append(SEPAR_CHARAC);

			oneLine.append(product.getPrixVente());
			oneLine.append(SEPAR_CHARAC);

			oneLine.append(product.getPrixSolde());
			oneLine.append(SEPAR_CHARAC);
			

			oneLine.append(product.getOtherPriceNew());
			oneLine.append(SEPAR_CHARAC);
			
			oneLine.append(product.getOtherPriceUsed());
			oneLine.append(SEPAR_CHARAC);
			

			oneLine.append(product.getClassement());
			oneLine.append(SEPAR_CHARAC);
			

			oneLine.append(product.getRatingMean());
			oneLine.append(SEPAR_CHARAC);
			

			
			oneLine.append(product.getNbComment());
			oneLine.append(SEPAR_CHARAC);
			

			oneLine.append(product.getIsDisponible());
			oneLine.append(SEPAR_CHARAC);
			
			

			oneLine.append(product.getIsSoldbyAmazon());
			oneLine.append(SEPAR_CHARAC);
			

			oneLine.append(product.getAsinRecommandationProduct());	
			oneLine.append(SEPAR_CHARAC);
			
			oneLine.append(product.getKeyWords());	
			
			
			bw.write(oneLine.toString());
			bw.newLine();

			bw.flush();
			bw.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		} 
	}
}