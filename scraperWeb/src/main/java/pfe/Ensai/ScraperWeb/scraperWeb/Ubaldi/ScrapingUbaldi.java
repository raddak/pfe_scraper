package pfe.Ensai.ScraperWeb.scraperWeb.Ubaldi;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pfe.Ensai.ScraperWeb.scraperWeb.Interfaces.IScraperProduct;
import pfe.Ensai.ScraperWeb.scraperWeb.Produit.ProduitD;

// On utilise l'interface IScraperProduct qui est importante pour la fonction startScraper
public class ScrapingUbaldi implements IScraperProduct {




	private static void print(String msg, Object... url) {
		System.out.println(String.format(msg, url	));
	}

	private static String trim(String s, int width) {
		if (s.length() > width)
			return s.substring(0, width-1) + ".";
		else
			return s;

	}


	public static void produitsToCsv(ArrayList<String> liens) throws IOException{

		FileWriter writer = new FileWriter("produitsUbaldi.csv", true);

		writer.append("Site").append("\t");
		writer.append("URL").append("\t");
		writer.append("CodeEAN").append("\t");
		writer.append("Nom").append("\t");
		writer.append("Categorie").append("\t");
		writer.append("SsCategorie").append("\t");
		writer.append("Marque").append("\t");
		writer.append("Description").append("\t");
		writer.append("PrixOrigine").append("\t");
		writer.append("PrixVente").append("\t");
		writer.append("EnStock").append("\t");
		writer.append("NoteMoyenne").append("\t");
		writer.append("NombreAvis").append("\n");

		for(String lien : liens){
			produitToCsv(lien, writer);
		}


		writer.close();

	}


	private static void produitToCsv(String url, FileWriter writer) throws IOException{


		System.out.println("Produit : " + url);
		Document doc = Jsoup.connect(url).timeout(0).get(); 
		Elements nom = doc.select("h1");
		Elements prix = doc.select("span[class*=rebours-prix]");
		Elements prixNonDispo = doc.select("span[class*=clignote]");
		String prixOrigine;
		String prixVente;
		Elements imgStock = doc.select("td[class*=f_a_br]");
		String enStock = "false";
		Elements categories = doc.select("div[class*=breadCrumb]").get(0).getElementsByTag("a");
		Elements nonNote = doc.select("p[class*=pr-snapshot-no-ratings]");
		String note="";
		String ean = "";
		Elements avis = doc.select("span[class*=count]");
		Elements descriptionE = doc.select("p[itemprop*=description]");
		String description="";
		String categorie = "";
		String ssCategorie = "";
		String marque = "";
		
		if (descriptionE.size()>0) {
			
			description=descriptionE.get(0).text();
			description=description.replaceAll("\"", "'");
			System.out.println(description);
		}
		
		String nbAvis="0";


		if(prix.size()>0){
			prixOrigine=prix.get(0).attr("data-prix-origine");
			prixVente=prix.get(0).attr("data-prix-vente");
		} else {
			if(prixNonDispo.size() > 0){
				prixOrigine=prixNonDispo.get(0).getAllElements().get(1).ownText();
				prixVente="";
			} else {
				prixOrigine="";
				prixVente="";
			}
		}

			if(doc.select("span[itemprop*=average]").size()>0){
			note=doc.select("span[itemprop*=average]").get(0).ownText();
			}
			if(avis.size()>0){
			nbAvis=avis.get(0).ownText();
			}
		

		if(imgStock.size()>1){
		 if(imgStock.get(1).getElementsByTag("img").get(0).attr("src").equalsIgnoreCase("http://medias3-2.ubaldi.com/html_imgs_12/dispo/dispo.jpg")){
			enStock="true";
		 }
		}
		
		if(doc.select("div[id*=dv_ean]").size()>0){
			ean = doc.select("div[id*=dv_ean]").get(0).attr("title");
		}
		
		
		
		if(categories.size()>4){
			categorie = categories.get(2).attr("title");
			ssCategorie = categories.get(3).attr("title"); 
			marque = categories.get(4).attr("title");
		}
		
		writer.append("Ubaldi").append("\t");
		writer.append(url).append("\t");
		writer.append(ean).append("\t");
		writer.append("\"" + nom.get(0).ownText() + "\"").append("\t");
		writer.append(categorie).append("\t");
		writer.append(ssCategorie).append("\t");
		writer.append(marque).append("\t");
		writer.append("\"" + description + "\"").append("\t");
		writer.append(prixOrigine).append("\t");
		writer.append(prixVente).append("\t");
		writer.append(enStock).append("\t");
		writer.append(note).append("\t");
		writer.append(nbAvis).append("\n");




	}





	/**
	 * Stocke les liens HTML de tous les produits de la catégorie "puericulture" dans une Liste de String.
	 * @return liste des liens HTML
	 * @throws IOException
	 */
	public static ArrayList<String> getLiensProduits() throws IOException{
		String prefixeUrl="http://www.ubaldi.com/ubaldi/nos-references";
		char lettre = 'a';
		ArrayList<String> liens = new ArrayList<String>();

		//pour références commençant par a
		getLiensProduitsLettre(liens,prefixeUrl+".php");

		//pour références commençant par b,..,z
		for(int i = 1; i < 26; i++){
			lettre++;
			getLiensProduitsLettre(liens,prefixeUrl+"-"+lettre+".php");

		}

		//pour références commençant par 0,..,9
		for(int j=0; j < 10; j++){
			getLiensProduitsLettre(liens,prefixeUrl+"-"+j+".php");
		}

		return liens;
	}

	private static void getLiensProduitsLettre(ArrayList<String> liens,String url) throws IOException{


		Document doc = Jsoup.connect(url).timeout(0).get();
		Elements aLiens = doc.select("div[class*=bloc_ref] > a");

		for (Element aLien : aLiens) {
			String lien = aLien.attr("href");

			//puericulture: la seule catégorie du site commençant par 'pue'
			if(lien.substring(1, 4).equalsIgnoreCase("pue")){
				liens.add("http://www.ubaldi.com"+lien);
			}
		}

	}


	/** Fonction permettant à partir de l'url d'un produit de récupérer les données et renvoyant un produit.
	 * 
	 * @param url
	 * @return
	 */
	public ProduitD startScraper(String url) {
		ProduitD prod = new ProduitD(url, "Ubaldi");
		if (url != "") {
			try {


				Document doc = Jsoup.connect(url).timeout(0).get(); 
				Elements nom = doc.select("h1");
				Elements prix = doc.select("span[class*=rebours-prix]");
				Elements prixNonDispo = doc.select("span[class*=clignote]");
				String prixOrigine;
				String prixVente;
				Elements imgStock = doc.select("td[class*=f_a_br]");
				boolean enStock = false;
				Elements categories = doc.select("div[class*=breadCrumb]").get(0).getElementsByTag("a");
				Elements nonNote = doc.select("p[class*=pr-snapshot-no-ratings]");
				String note="";
				Elements avis = doc.select("span[class*=count]");
				String ean = "";
				String categorie = "";
				String ssCategorie = "";
				String marque = "";
				Elements descriptionE = doc.select("p[itemprop*=description]");
				String description="";
				
				if (descriptionE.size()>0) {
					
					description=descriptionE.get(0).text();

					description=description.replaceAll("\"", "'");
					
					System.out.println(description);
				}
				
				String nbAvis="0";
				
				if(prix.size()>0){
					prixOrigine=prix.get(0).attr("data-prix-origine");
					prixVente=prix.get(0).attr("data-prix-vente");
				} else {
					if(prixNonDispo.size() > 0){
						prixOrigine=prixNonDispo.get(0).getAllElements().get(1).ownText();
						prixVente="";
					} else {
						prixOrigine="";
						prixVente="";
					}
				}

					if(doc.select("span[itemprop*=average]").size()>0){
						note=doc.select("span[itemprop*=average]").get(0).ownText();
						}
					if(avis.size()>0){
					nbAvis=avis.get(0).ownText();
					}
					
				
				if(imgStock.size()>1){
					 if(imgStock.get(1).getElementsByTag("img").get(0).attr("src").equalsIgnoreCase("http://medias3-2.ubaldi.com/html_imgs_12/dispo/dispo.jpg")){
						enStock=true;
					 }
					}

				Double prixV=null;

				if (prixVente!=null && !prixVente.isEmpty()) {

					prixV = Double.parseDouble(prixVente);
				}

				if(doc.select("div[id*=dv_ean]").size()>0){
					ean = doc.select("div[id*=dv_ean]").get(0).attr("title");
				}
				
				if(categories.size()>4){
					categorie = categories.get(2).attr("title");
					ssCategorie = categories.get(3).attr("title"); 
					marque = categories.get(4).attr("title");
				}

				prod.setDescription(description);
				prod.setNom(nom.get(0).ownText());
				prod.setMarque(marque);
				prod.setEAN(ean);
				prod.setPrixVente(prixV);
				prod.setIsDisponible(enStock);
				prod.setCategorie(categorie);
				prod.setSousCategorie(ssCategorie);
				prod.setRatingMean(note);
				prod.setNbComment(nbAvis);

			}

			catch (IOException e1) {
				System.out.println("StartScraper n'a pas pu se connecter");
				e1.printStackTrace();
				prod = null;

			}
		}
		return prod.getNom() != null ? prod : null;

	}


}
